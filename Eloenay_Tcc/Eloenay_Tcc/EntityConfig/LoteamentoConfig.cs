﻿using Eloenay_Tcc.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Eloenay_Tcc.EntityConfig
{
    public class LoteamentoConfig : EntityTypeConfiguration<Loteamento>
    {
        public LoteamentoConfig()
        {
            HasKey(c => c.LoteamentoId);
            Property(c => c.LoteamentoId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.Nome).IsRequired().HasMaxLength(100);
            Property(c => c.Bairro).IsRequired().HasMaxLength(40);
            Property(c => c.Cep).IsRequired().HasMaxLength(9);
            Property(c => c.Endereco).IsRequired();
            Property(c => c.Cidade).IsRequired();
            Property(c => c.Matricula).HasMaxLength(20).IsRequired();
            Property(c => c.MetrosQuadrados).IsRequired();
            Property(c => c.Numero).IsRequired();
            Property(c => c.Estado).IsRequired();

            HasRequired(c => c.Proprietario).WithMany(c => c.Loteamentos).HasForeignKey(c => c.ProprietarioId).WillCascadeOnDelete(false);
            HasMany(c => c.Quadras).WithRequired(c => c.Loteamento);

            ToTable(nameof(Loteamento));
        }
    }
}