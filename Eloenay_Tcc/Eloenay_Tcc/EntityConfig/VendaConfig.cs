﻿using Eloenay_Tcc.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Eloenay_Tcc.EntityConfig
{
    public class VendaConfig : EntityTypeConfiguration<Venda>
    {
        public VendaConfig()
        {
            HasKey(c => c.VendaId);
            Property(c => c.VendaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(v => v.DataDaCompra).IsRequired();
            Property(v => v.DataDoCancelamento);
            Property(v => v.Juros).IsRequired();
            Property(v => v.MultaDeRescisao).IsRequired();
            Property(v => v.QuantidadeDeParcelas).IsRequired();
            Property(v => v.StatusDaVenda).IsRequired();
            Property(v => v.ValorDaEntrada).IsRequired();

            HasRequired(q => q.Cliente).WithMany(q => q.Vendas).HasForeignKey(q => q.ClienteId).WillCascadeOnDelete(false);
            HasRequired(q => q.Lote).WithMany(q => q.Vendas).HasForeignKey(q => q.LoteId).WillCascadeOnDelete(false);
            HasMany(q => q.Parcelas).WithRequired(q => q.Venda);

            ToTable(nameof(Venda));
        }
    }
}