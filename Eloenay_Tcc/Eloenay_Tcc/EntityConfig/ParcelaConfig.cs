﻿using Eloenay_Tcc.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Eloenay_Tcc.EntityConfig
{
    public class ParcelaConfig : EntityTypeConfiguration<Parcela>
    {
       public ParcelaConfig()
        {
            HasKey(c => c.ParcelaId);
            Property(c => c.ParcelaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.DataDaParcela).IsRequired();
            Property(c => c.ValorDaParcela).IsRequired();
            Property(c => c.ValorPagoDaParcela);
            Property(c => c.StatusDaParcela).IsRequired();

            HasRequired(q => q.Venda).WithMany(q => q.Parcelas).HasForeignKey(q => q.VendaId).WillCascadeOnDelete(false);

            ToTable(nameof(Parcela));
        }
    }
}