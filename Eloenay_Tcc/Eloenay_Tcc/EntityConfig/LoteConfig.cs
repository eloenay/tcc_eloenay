﻿using Eloenay_Tcc.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Eloenay_Tcc.EntityConfig
{
    public class LoteConfig : EntityTypeConfiguration<Lote>
    {
        public LoteConfig()
        {
            HasKey(l => l.LoteId);
            Property(l => l.LoteId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(l => l.Comprimento).IsRequired();
            Property(l => l.IdentificacaoDoLote).IsRequired().HasMaxLength(10);
            Property(l => l.Largura).IsRequired();
            Property(l => l.StatusDoLote).IsRequired();
            Property(l => l.MetroQuadradoLote).IsRequired();
            Property(l => l.ValorDoLote).IsRequired();

            HasRequired(l => l.Quadra).WithMany(l => l.Lotes).HasForeignKey(l => l.QuadraId).WillCascadeOnDelete(false);

            ToTable(nameof(Lote));
        }
    }
}