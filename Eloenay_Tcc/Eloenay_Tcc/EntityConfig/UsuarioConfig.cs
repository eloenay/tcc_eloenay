﻿using Eloenay_Tcc.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Eloenay_Tcc.EntityConfig
{
    public class UsuarioConfig : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfig()
        {
            HasKey(c => c.UsuarioId);
            Property(c => c.UsuarioId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(u => u.Login).IsRequired().HasMaxLength(100);
            Property(u => u.Nome).IsRequired().HasMaxLength(100);
            Property(u => u.Senha).IsRequired().HasMaxLength(100);
            Property(u => u.TipoUsuario).IsRequired();

            ToTable(nameof(Usuario));
        }
    }
}