﻿using Eloenay_Tcc.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Eloenay_Tcc.EntityConfig
{
    public class QuadraConfig : EntityTypeConfiguration<Quadra>
    {
        public QuadraConfig()
        {
            HasKey(q => q.QuadraId);
            Property(q => q.QuadraId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(q => q.Comprimento).IsRequired();
            Property(q => q.IdentificacaoDaQuadra).IsRequired().HasMaxLength(10);
            Property(q => q.Largura).IsRequired();
            Property(q => q.MetroQuadradoDaQuadra).IsRequired();

            HasRequired(q => q.Loteamento).WithMany(q => q.Quadras).HasForeignKey(q => q.LoteamentoId).WillCascadeOnDelete(false);
            HasMany(c => c.Lotes).WithRequired(c => c.Quadra);

            ToTable(nameof(Quadra));
        }
    }
}