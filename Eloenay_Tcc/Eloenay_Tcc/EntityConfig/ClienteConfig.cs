﻿using Eloenay_Tcc.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Eloenay_Tcc.EntityConfig
{
    public class ClienteConfig : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfig()
        {
            HasKey(c => c.ClienteId);
            Property(c => c.ClienteId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.Nome).IsRequired().HasMaxLength(100);
            Property(c => c.Cpf).IsRequired().HasMaxLength(14);
            Property(c => c.Rg).IsRequired();
            Property(c => c.OrgaoExpedidor).IsRequired().HasMaxLength(20);
            Property(c => c.Nacionalidade).IsRequired().HasMaxLength(20);
            Property(c => c.Ocupacao).IsRequired().HasMaxLength(100);
            Property(c => c.Endereco).IsRequired().HasMaxLength(100);
            Property(c => c.Numero).IsRequired();
            Property(c => c.Bairro).IsRequired().HasMaxLength(100);
            Property(c => c.Cep).IsRequired().HasMaxLength(9);
            Property(c => c.Estado).IsRequired();
            Property(c => c.EstadoCivil).IsRequired();
            Property(c => c.Cidade).IsRequired().HasMaxLength(100);

            HasRequired(x => x.Usuario);

            ToTable(nameof(Cliente));
        }
    }
}