using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;

namespace Eloenay_Tcc.Models
{
    public class Loteamento
    {
        #region Atributos
        public Guid LoteamentoId { get; set; }
        public string Nome { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string Endereco { get; set; }
        public string Matricula { get; set; }
        public decimal MetrosQuadrados { get; set; }
        public int Numero { get; set; }
        public Estado Estado { get; set; }
        #endregion

        #region Relacionamentos
        public Guid ProprietarioId { get; set; }
        public Proprietario Proprietario { get; set; }
        #endregion

        #region Bidirecional
        [ScriptIgnore]
        public ICollection<Quadra> Quadras { get; set; }
        #endregion

        #region Crud
        LoteamentoRepository _loteamentoRepository = new LoteamentoRepository();
        public void Adicionar(Loteamento loteamento) => _loteamentoRepository.Adicionar(loteamento);
        public void Atualizar(Loteamento loteamento) => _loteamentoRepository.Atualizar(loteamento);
        public IEnumerable<Loteamento> BuscarTodos() => _loteamentoRepository.BuscarTodos();
        public Loteamento BuscarPorId(Guid id) => _loteamentoRepository.BuscarPorId(id);
        public IEnumerable<Loteamento> BuscarPorNome(string nome) => _loteamentoRepository.BuscarPorNome(nome);
        #endregion

        #region M�todos
        public int QuantidadesDeLotesDisponiveis(Guid id)
        {
            return Quadras?.Where(x => x.LoteamentoId == id).Sum(d => d.Lotes.Where(i => i.StatusDoLote == true).Count()) ?? 0;
        }
        public int QuantidadesDeLotesVendidos(Guid id)
        {
            return Quadras?.Where(x => x.LoteamentoId == id).Sum(d => d.Lotes.Where(i => i.StatusDoLote == false && i.Vendas.Any(v => v.Parcelas.Count(x => x.StatusDaParcela == true) != v.Parcelas.Count())).Count()) ?? 0;
        }
        #endregion
    }
}