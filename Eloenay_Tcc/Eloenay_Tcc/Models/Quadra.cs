using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Eloenay_Tcc.Models
{
    public class Quadra
    {
        #region Atributos
        public Guid QuadraId { get; set; }
        public string IdentificacaoDaQuadra { get; set; }
        public double Comprimento { get; set; }
        public double Largura { get; set; }
        public double MetroQuadradoDaQuadra { get; set; }
        #endregion

        #region Relacionamentos
        public Guid LoteamentoId { get; set; }
        public Loteamento Loteamento { get; set; }
        #endregion

        #region Bidirecional
        [ScriptIgnore]
        public ICollection<Lote> Lotes { get; set; }
        #endregion

        #region Crud
        QuadraRepository _quadraRepository = new QuadraRepository();
        public void Adicionar(Quadra quadra) => _quadraRepository.Adicionar(quadra);
        public void Atualizar(Quadra quadra) => _quadraRepository.Atualizar(quadra);
        public Quadra BuscarPorId(Guid quadraId) => _quadraRepository.BuscarPorId(quadraId);
        public IEnumerable<Quadra> BuscarPorNome(string nome) => _quadraRepository.BuscarPorNome(nome);
        public IEnumerable<Quadra> ObterQuadraPorLoteamento(Guid loteamentoId) => _quadraRepository.ObterQuadraPorLoteamento(loteamentoId);
        #endregion

        #region M�todos
        public double CalcularMetrosQuadrados(double largura, double comprimento)
        {
            MetroQuadradoDaQuadra = largura * comprimento;
            return MetroQuadradoDaQuadra;
        }
        #endregion

    }
}