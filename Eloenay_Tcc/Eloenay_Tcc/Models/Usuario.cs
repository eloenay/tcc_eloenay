﻿using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;

namespace Eloenay_Tcc.Models
{
    public class Usuario
    {
        #region Atributos
        public Guid UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public TipoUsuario TipoUsuario { get; set; }
        #endregion

        #region Bidirecional
        public ICollection<Venda> Vendas { get; set; }
        public ICollection<Cliente> Clientes { get; set; }
        public ICollection<Proprietario> Proprietarios { get; set; }
        #endregion

        #region Crud
        UsuarioRepository _usuarioRepositories = new UsuarioRepository();
        public void Adicionar(Usuario usuario) => _usuarioRepositories.Adicionar(usuario);
        public void Atualizar(Usuario usuario) => _usuarioRepositories.Atualizar(usuario);
        public Usuario BuscarPorId(Guid usuarioId) => _usuarioRepositories.BuscarPorId(usuarioId);
        public Usuario BuscarPorLogin(string login) => _usuarioRepositories.BuscarPorLogin(login);
        public IEnumerable<Usuario> BuscarTodos() => _usuarioRepositories.BuscarTodos();
        #endregion
    }
}
