using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;

namespace Eloenay_Tcc.Models
{
    public class Venda
    {
        #region Atributos
        public Guid VendaId { get; set; }
        public int Juros { get; set; }
        public int MultaDeRescisao { get; set; }
        public int QuantidadeDeParcelas { get; set; }
        public bool StatusDaVenda { get; set; }
        public decimal ValorDaEntrada { get; set; }
        public DateTime DataDaCompra { get; set; }
        public DateTime? DataDoCancelamento { get; set; }
        #endregion

        #region Relacionamento
        public Guid ClienteId { get; set; }
        public Cliente Cliente { get; set; }
        public Guid LoteId { get; set; }
        public Lote Lote { get; set; }
        public Guid UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        #endregion

        #region Bidirecional
        [ScriptIgnore]
        public ICollection<Parcela> Parcelas { get; set; }
        #endregion

        #region N�o Mapeado
        public string ValorEntradaFormat => string.Format("{0:#,0.00}", ValorDaEntrada);
        public string DataCompraFormat => $"{DataDaCompra.ToLongDateString()}";
        public string DataCancelamentoFormat => $"{DataDoCancelamento?.ToLongDateString()}";
        public int? ParcelaPaga => Parcelas.Count(x => x.StatusDaParcela == true);
        public decimal? ValorPago => Parcelas.Where(x => x.StatusDaParcela == true).Sum(x => x.ValorPagoDaParcela);
        public decimal? ValorDevolucao => ValorParaDevolucao();
        public decimal? ValorDescontado => ValorParaDesconto();
        #endregion

        #region Crud
        VendaRepository _vendaRepository = new VendaRepository();
        public void Adicionar(Venda venda) => _vendaRepository.Adicionar(venda);
        public void Atualizar(Venda venda) => _vendaRepository.Atualizar(venda);
        public Venda BuscarPorId(Guid vendaId) => _vendaRepository.BuscarPorId(vendaId);
        public IEnumerable<Venda> BuscarPorNome(string nome) => _vendaRepository.BuscarPorNome(nome);
        public IEnumerable<Venda> BuscarTodos() => _vendaRepository.BuscarTodos();
        public IEnumerable<Venda> BuscarTodasPorLoteamento(Guid id) => _vendaRepository.BuscarTodasPorLoteamento(id);
        #endregion

        #region M�todos
        public void AtribuirValores(Venda venda)
        {
            venda.DataDaCompra = DateTime.Now;
            venda.StatusDaVenda = true;
        }

        public void AtualizarStatus()
        {
            StatusDaVenda = !StatusDaVenda;
            DataDoCancelamento = DateTime.Now;
        }

        public int QuantidadesDeVendasCanceladas(Guid id) => _vendaRepository.QuantidadesDeVendasCanceladas(id);
        public int QuantidadesDeVendasFinalizada(Guid id) => _vendaRepository.QuantidadesDeVendasFinalizada(id);

        public decimal ValorParaDevolucao()
        {
            var valor = Parcelas.Where(x => x.StatusDaParcela == true).Sum(x => x.ValorPagoDaParcela);
            var porcentagemParaDevolucao = (100 - MultaDeRescisao);
            var resultado = (valor * porcentagemParaDevolucao) / 100;
            return resultado;
        }

        public decimal ValorParaDesconto()
        {
            var valor = Parcelas.Where(x => x.StatusDaParcela == true).Sum(x => x.ValorPagoDaParcela);
            var resultado = (valor * MultaDeRescisao) / 100;
            return resultado;
        }
        #endregion
    }
}