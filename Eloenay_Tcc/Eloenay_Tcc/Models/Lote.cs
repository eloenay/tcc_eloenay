using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Eloenay_Tcc.Models
{
    public class Lote
    {
        #region Atributos
        public Guid LoteId { get; set; }
        public double Comprimento { get; set; }
        public string IdentificacaoDoLote { get; set; }
        public double Largura { get; set; }
        public double MetroQuadradoLote { get; set; }
        public decimal ValorDoLote { get; set; }
        public bool StatusDoLote { get; set; } = true;
        #endregion

        #region Relacionamenetos
        public Guid QuadraId { get; set; }
        public Quadra Quadra { get; set; }
        #endregion

        #region Bidirecional
        public ICollection<Venda> Vendas { get; set; }
        #endregion

        #region Crud
        LoteRepository _loteRepository = new LoteRepository();
        public void Adicionar(Lote lote) => _loteRepository.Adicionar(lote);
        public void Atualizar(Lote lote) => _loteRepository.Atualizar(lote);
        public Lote BuscarPorId(Guid loteId) => _loteRepository.BuscarPorId(loteId);
        public IEnumerable<Lote> BuscarPorNome(string nome) => _loteRepository.BuscarPorNome(nome);
        public IEnumerable<Lote> ObterLotePorQuadra(Guid quadraId) => _loteRepository.ObterLotePorQuadra(quadraId);
        public IEnumerable<Lote> ObterLotePeloLoteamento(Guid loteamentoId) => _loteRepository.ObterLotePeloLoteamento(loteamentoId);
        public IEnumerable<Lote> ObterLotesVendidosPeloLoteamento(Guid loteamentoId) => _loteRepository.ObterLotesVendidosPeloLoteamento(loteamentoId);
        public IEnumerable<Lote> BuscarLote(Guid id) => _loteRepository.BuscarLote(id);
        #endregion

        #region N�o Mapeados
        public string ValorLoteFormat => string.Format("{0:#,0.00}", ValorDoLote);
        #endregion

        #region M�todos
        public double CalcularMetrosQuadrados(double largura, double comprimento)
        {
            MetroQuadradoLote = largura * comprimento;
            return MetroQuadradoLote;
        }

        public void AtualizarStatus()
        {
            StatusDoLote = !StatusDoLote;
        }

        public void AtualizarDados(Lote lote)
        {
            IdentificacaoDoLote = lote.IdentificacaoDoLote;
            Largura = lote.Largura;
            Comprimento = lote.Comprimento;
            MetroQuadradoLote = lote.MetroQuadradoLote;
            ValorDoLote = lote.ValorDoLote;
        }

        public int QuantidadesDeLotes(IEnumerable<Lote> lotes) => lotes.Count();
        public decimal ValorTotalDosLotes(IEnumerable<Lote> lotes) => lotes.Sum(x => x.ValorDoLote);

        public decimal ValorTotalDosLotesEmVendasRealizadas(IEnumerable<Venda> venda)
        {
            return venda.Select(x => x.Lote).Where(x => x.StatusDoLote == false && x.Vendas.Any(v => v.StatusDaVenda == true && v.Parcelas.Count(p => p.StatusDaParcela == false) != 0)).Sum(x => x.ValorDoLote);
        }

        public decimal ValorTotalDosLotesDisponiveis(IEnumerable<Lote> lote)
        {
            return lote.Where(x => x.StatusDoLote == true).Sum(x => x.ValorDoLote);
        }

        public decimal ValorTotalDosLotesEmVendasFinalizadas(IEnumerable<Venda> venda)
        {
            return venda.Select(x => x.Lote).Where(x => x.StatusDoLote == false && x.Vendas.Any(v => v.StatusDaVenda == true && v.Parcelas.Count(p => p.StatusDaParcela == false) == 0)).Sum(x => x.Vendas.Sum(s => s.Parcelas.Sum(p => p.ValorPagoDaParcela)));
        }

        public decimal ValorTotalDosLotesDasVendasCanceladas(IEnumerable<Venda> venda)
        {
            return venda.Select(x => x.Lote).Where(x => x.Vendas.Any(v => v.StatusDaVenda == false)).Sum(x => x.ValorDoLote);
        }
        #endregion
    }
}