﻿using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;

namespace Eloenay_Tcc.Models
{
    public class Parcela
    {
        #region Atributos
        public Guid ParcelaId { get; set; }
        public int IdentificacaoParcela { get; set; }
        public decimal ValorDaParcela { get; set; }
        public decimal ValorPagoDaParcela { get; set; }
        public bool StatusDaParcela { get; set; }
        public DateTime DataDaParcela { get; set; }
        #endregion

        #region Relacionamentos
        public Guid VendaId { get; set; }
        [ScriptIgnore]
        public Venda Venda { get; set; }
        #endregion

        #region Crud
        ParcelaRepository _parcelaRepository = new ParcelaRepository();
        public void Adicionar(Parcela parcelas) => _parcelaRepository.Adicionar(parcelas);
        public void Atualizar(Parcela parcelas) => _parcelaRepository.Atualizar(parcelas);
        public Parcela BuscarPorId(Guid parcelas) => _parcelaRepository.BuscarPorId(parcelas);
        public IEnumerable<Parcela> BuscarPorNome(string nome) => _parcelaRepository.BuscarPorNome(nome);
        public IEnumerable<Parcela> BuscarTodosPorVenda(Guid id) => _parcelaRepository.BuscarTodosPorVenda(id);
        public IEnumerable<Parcela> BuscarTodosPorLoteamento(Guid id) => _parcelaRepository.BuscarTodosPorLoteamento(id);
        public IEnumerable<Parcela> BuscarTodosPorLoteamentoPeriodo(Guid id, int ano, int mes) => _parcelaRepository.BuscarTodosPorLoteamentoPeriodo(id, ano, mes);

        #endregion

        #region Não Mapeados
        public string DataParcelaFormt => DataDaParcela.ToShortDateString();
        public string ValorParcelaFormt => string.Format("{0:#,0.00}", ValorDaParcela);
        public string ValorPagoParcelaFormt => string.Format("{0:#,0.00}", ValorPagoDaParcela);
        #endregion

        #region Métodos
        public decimal ValorDasParcelas(decimal valorLote, decimal valorEntrada, int quantParcelas, double juros)
        {
            decimal jurosConvert = (decimal)juros / 100;
            decimal calculoValorJurosPorMes;
            decimal calculoFinalValorLoteEntradaJuros;
            decimal total;

            if (juros == 0)
            {
                calculoValorJurosPorMes = valorLote + jurosConvert;
            }
            else
            {
                calculoValorJurosPorMes = (valorLote * jurosConvert) + valorLote;
            }
            calculoFinalValorLoteEntradaJuros = calculoValorJurosPorMes - valorEntrada;
            total = calculoFinalValorLoteEntradaJuros / quantParcelas;
            return total;
        }

        public void CriarParcelas(Guid vendaId, DateTime primeiraParcela, int quantParcelas, decimal valorParcelas)
        {
            var dataFim = primeiraParcela;
            var dataInicio = dataFim;
            for (int i = 0; i < quantParcelas; i++)
            {
                var parcelas = new Parcela
                {
                    ParcelaId = Guid.NewGuid(),
                    ValorDaParcela = valorParcelas,
                    IdentificacaoParcela = i + 1,
                    StatusDaParcela = false,
                    DataDaParcela = dataFim.AddMonths(i),
                    VendaId = vendaId
                };
                dataInicio = dataFim;
                Adicionar(parcelas);
            }
        }

        public void AtualizarStatus(bool statusParcela)
        {
            StatusDaParcela = !statusParcela;
        }

        public Parcela ObterValorParcela(Guid id) => _parcelaRepository.ObterValorParcela(id);

        public decimal ObterValorTotalDeParcelas(IEnumerable<Parcela> parcela) => parcela.Where(x => x.Venda.StatusDaVenda == true).Sum(x => x.ValorDaParcela);
        public int ContagemDeTodasAsParcelas(IEnumerable<Parcela> parcela) => parcela.Count(x => x.Venda.StatusDaVenda == true);
        public int ContagemDeParcelasPagas(IEnumerable<Parcela> parcela) => parcela.Count(x => x.Venda.StatusDaVenda == true && x.StatusDaParcela == true);
        public int ContagemDeParcelasPendentes(IEnumerable<Parcela> parcela) => parcela.Count(x => x.Venda.StatusDaVenda == true && x.StatusDaParcela == false && x.DataDaParcela >= DateTime.Now);
        public int ContagemDeParcelasAtrasadas(IEnumerable<Parcela> parcela) => parcela.Count(x => x.Venda.StatusDaVenda == true && x.StatusDaParcela == false && x.DataDaParcela < DateTime.Now);
        public decimal ObterValorDasParcelasPagas(IEnumerable<Parcela> parcela) => parcela.Where(x => x.Venda.StatusDaVenda == true).Sum(x => x.ValorPagoDaParcela);
        public decimal ObterValorDasParcelasPendentes(IEnumerable<Parcela> parcela) => parcela.Where(x => x.Venda.StatusDaVenda == true && x.StatusDaParcela == false && x.DataDaParcela >= DateTime.Now).Sum(x => x.ValorDaParcela);
        public decimal ObterValorDasParcelasAtrasadas(IEnumerable<Parcela> parcela) => parcela.Where(x => x.Venda.StatusDaVenda == true && x.StatusDaParcela == false && x.DataDaParcela < DateTime.Now).Sum(x => x.ValorDaParcela);
        public decimal ObterValorTotalDeParcelasEmVendasCanceladas(IEnumerable<Parcela> parcela) => parcela.Where(x => x.Venda.StatusDaVenda == false).Sum(x => x.ValorDaParcela);
        public int ContagemDeTodasAsParcelasEmVendasCanceladas(IEnumerable<Parcela> parcela) => parcela.Count(x => x.Venda.StatusDaVenda == false);
        public int ContagemDeParcelasPagasEmVendasCanceladas(IEnumerable<Parcela> parcela) => parcela.Count(x => x.Venda.StatusDaVenda == false && x.StatusDaParcela == true);
        public decimal ObterValorDasParcelasPagasEmVendasCanceladas(IEnumerable<Parcela> parcela) => parcela.Where(x => x.Venda.StatusDaVenda == false).Sum(x => x.ValorPagoDaParcela);
        public decimal TotalDeValorRestituido(IEnumerable<Parcela> parcelas)
        {
            var valorParcelas = ObterValorDasParcelasPagasEmVendasCanceladas(parcelas);
            var resultado = (valorParcelas * 70) / 100;
            return resultado;
        }
        #endregion
    }
}