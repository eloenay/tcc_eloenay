using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Eloenay_Tcc.Models
{
    public class Cliente
    {
        #region Atributos
        public Guid ClienteId { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public int Rg { get; set; }
        public string OrgaoExpedidor { get; set; }
        public string Nacionalidade { get; set; }
        public EstadoCivil EstadoCivil { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public int Numero { get; set; }
        public string Ocupacao { get; set; }
        public Estado Estado { get; set; }
        #endregion

        #region Relacionamentos
        public Guid UsuarioId { get; set; }
        [ScriptIgnore]
        public Usuario Usuario { get; set; } 
        #endregion

        #region Bidirecional
        [ScriptIgnore]
        public ICollection<Venda> Vendas { get; set; }
        #endregion

        #region Crud
        ClienteRepository _clienteRepository = new ClienteRepository();
        public void Adicionar(Cliente cliente) => _clienteRepository.Adicionar(cliente);
        public void Atualizar(Cliente cliente) => _clienteRepository.Atualizar(cliente);
        public Cliente BuscarPorId(Guid id) => _clienteRepository.BuscarPorId(id);
        public IEnumerable<Cliente> BuscarPorNome(string nome) => _clienteRepository.BuscarPorNome(nome);
        #endregion

        #region N�o Mapeados
        public string DataNascimentoFormt => DataNascimento.ToShortDateString();
        #endregion
    }
}