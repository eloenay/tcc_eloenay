using Eloenay_Tcc.Repository;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Eloenay_Tcc.Models
{
    public class Proprietario
    {
        #region Atributos
        public Guid ProprietarioId { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public int Rg { get; set; }
        public string OrgaoExpedidor { get; set; }
        public string Nacionalidade { get; set; }
        public EstadoCivil EstadoCivil { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public int Numero { get; set; }
        public string Ocupacao { get; set; }
        public Estado Estado { get; set; }
        #endregion

        #region Relacionamentos
        public Guid UsuarioId { get; set; }
        [ScriptIgnore]
        public Usuario Usuario { get; set; } 
        #endregion

        #region Bidirecional
        [ScriptIgnore]
        public ICollection<Loteamento> Loteamentos { get; set; }
        #endregion

        #region N�o Mapedos
        public string DataNascimentoFormt => DataNascimento.ToShortDateString();
        #endregion

        #region Crud
        ProprietarioRepository _proprietarioRepository = new ProprietarioRepository();
        public void Adicionar(Proprietario proprietario) => _proprietarioRepository.Adicionar(proprietario);
        public void Atualizar(Proprietario proprietario) => _proprietarioRepository.Atualizar(proprietario);
        public Proprietario BuscarPorId(Guid proprietarioId) => _proprietarioRepository.BuscarPorId(proprietarioId);
        public IEnumerable<Proprietario> BuscarPorNome(string nome) => _proprietarioRepository.BuscarPorNome(nome);
        public IEnumerable<Proprietario> BuscarTodos() => _proprietarioRepository.BuscarTodos();
        #endregion
    }
}