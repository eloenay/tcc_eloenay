﻿using System.ComponentModel.DataAnnotations;

namespace Eloenay_Tcc.Models
{
    public enum EstadoCivil
    {
        SOLTEIRO = 1,
        CASADO = 2,
        DIVORCIADO = 3,
        [Display(Name = "VIÚVO")]
        VIUVO = 4,
    }

    public enum TipoUsuario
    {
        Administrador = 1,
        [Display(Name = "Usuário")]
        usuario = 2
    }

    public enum Estado
    {
        [Display(Name = "ACRE")]
        AC = 1,
        [Display(Name = "ALAGOAS")]
        AL = 2,
        [Display(Name = "AMAZONAS")]
        AM = 3,
        [Display(Name = "AMAPÁ")]
        AP = 4,
        [Display(Name = "BAHIA")]
        BA = 5,
        [Display(Name = "CEARÁ")]
        CE = 6,
        [Display(Name = "DISTRITO FEDERAL")]
        DF = 7,
        [Display(Name = "ESPIRÍTO SANTO")]
        ES = 8,
        [Display(Name = "GOIAS")]
        GO = 9,
        [Display(Name = "MARANHÃO")]
        MA = 10,
        [Display(Name = "MINAS GERAIS")]
        MG = 11,
        [Display(Name = "MATO GROSSO")]
        MT = 12,
        [Display(Name = "MATO GROSSO DO SUL")]
        MS = 13,
        [Display(Name = "PARÁ")]
        PA = 14,
        [Display(Name = "PARAÍBA")]
        PB = 15,
        [Display(Name = "PERNAMBUCO")]
        PE = 16,
        [Display(Name = "PIAUÍ")]
        PI = 17,
        [Display(Name = "PARANÁ")]
        PR = 18,
        [Display(Name = "RIO DE JANEIRO")]
        RJ = 19,
        [Display(Name = "RIO GRANDE DO NORTE")]
        RN = 20,
        [Display(Name = "RONDÔNIA")]
        RO = 21,
        [Display(Name = "RORAIMA")]
        RR = 22,
        [Display(Name = "RIO GRANDE DO SUL")]
        RS = 23,
        [Display(Name = "SANTA CATARINA")]
        SC = 24,
        [Display(Name = "SERGIPE")]
        SE = 25,
        [Display(Name = "SÃO PAULO")]
        SP = 26,
        [Display(Name = "TOCANTINS")]
        TO = 27
    }
}