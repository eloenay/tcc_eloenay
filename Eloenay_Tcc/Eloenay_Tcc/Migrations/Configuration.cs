using Eloenay_Tcc.Models;
using System;
using System.Data.Entity.Migrations;

namespace Eloenay_Tcc.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Eloenay_Tcc.Context.ContextBanco>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Eloenay_Tcc.Context.ContextBanco context)
        {
            context.Usuarios.AddOrUpdate(u => u.UsuarioId,
                new Usuario()
                {
                    UsuarioId = Guid.NewGuid(),
                    Nome = "ELOENAY PEREIRA",
                    Login = "admin",
                    Senha = "8D969EEF6ECAD3C29A3A629280E686CFC3F5D5A86AFF3CA122C923ADC6C92",//senha = 123456
                    TipoUsuario = TipoUsuario.Administrador
                });
        }
    }
}
