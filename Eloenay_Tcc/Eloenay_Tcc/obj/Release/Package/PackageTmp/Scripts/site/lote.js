﻿$(function () {

    "use string"

    window.Lote = window.Lote || {};

    Lote.Apoio = {
        CarregarEventos: () => {
            $(document).on("change", "#QuadraId", function () {
                var quadraId = $("#QuadraId").val();
                var url = $("#tabelaLote").data("url");
                $("#tabelaLote").load(url, { id: quadraId });
            });
        },
    };

    Lote.Apoio.CarregarEventos();
});