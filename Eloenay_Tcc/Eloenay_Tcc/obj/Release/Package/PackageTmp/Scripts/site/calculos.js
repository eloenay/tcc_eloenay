﻿$(function () {

    "use string"

    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    Number.prototype.formatMedida = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 20 ? j % 20 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{20})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    window.Calculos = window.Calculos || {};

    Calculos.MetrosQuadradosQuadra = function () {
        var largura = parseFloat($("#Largura").val());
        var comprimento = parseFloat($("#Comprimento").val());
        var resultado = largura * comprimento;
        document.getElementById("MetroQuadradoDaQuadra").value = resultado.formatMedida(2, ',', '.');
    };

    Calculos.MetrosQuadradosLote = function () {
        var largura = parseFloat($("#Largura").val());
        var comprimento = parseFloat($("#Comprimento").val());
        var resultado = largura * comprimento;
        document.getElementById("MetroQuadradoLote").value = resultado.formatMedida(2, ',', '.');
    };

    Calculos.Venda = function () {
        var valorLote = parseFloat($("#valorDoLote").val());
        var valorEntrada = parseFloat($("#ValorEntrada").val());
        var quantParcelas = parseFloat($("#QuantidadeDeParcelas").val());
        var juros = parseFloat($("#Juros").val()) / 100;

        if (juros === 0) {
            var calculoValorJurosPorMes = valorLote + juros;
        }
        else {
            var calculoValorJurosPorMes = (valorLote * juros) + valorLote;
        }       
        var calculoFinalValorLoteEntradaJuros = calculoValorJurosPorMes - valorEntrada;
        var total = (calculoFinalValorLoteEntradaJuros / quantParcelas).formatMoney(2, ',', '.');
        $("#valorParcelas").val(total);
    };

    $(document).on("keyup", "#Juros", function () {
        Calculos.Venda();
    });

    $(document).on("keyup", "#ValorEntrada", function () {
        Calculos.Venda();
    });

    $(document).on("keyup", "#QuantidadeDeParcelas", function () {
        Calculos.Venda();
    });
}); 