﻿$(function () {

    "use string"

    window.MenuLayout = window.MenuLayout || {};

    MenuLayout.Apaio = {

        CarregarEvento: () => {

            $("#btnCliente").click(function () {
                var url = $("#btnCliente").data("url");
                window.location.replace(url);
            });

            $("#btnProprietario").click(function () {
                var url = $("#btnProprietario").data("url");
                window.location.replace(url);
            });

            $("#btnLoteamento").click(function () {
                var url = $("#btnLoteamento").data("url");
                window.location.replace(url);
            });

            $("#btnQuadra").click(function () {
                var url = $("#btnQuadra").data("url");
                window.location.replace(url);
            });

            $("#btnLote").click(function () {
                var url = $("#btnLote").data("url");
                window.location.replace(url);
            });

            $("#btnVenda").click(function () {
                var url = $("#btnVenda").data("url");
                window.location.replace(url);
            });

            $("#btnParcela").click(function () {
                var url = $("#btnParcela").data("url");
                window.location.replace(url);
            });
        },
    };

    MenuLayout.Apaio.CarregarEvento();
});