﻿$(function () {

    "use strict";

    window.SGVL = window.SGVL || {};

    if ($(".sgvl-info-menu").length) {
        var item = $(".sgvl-info-menu > a.item[href$='" + window.location.pathname + "']").addClass("active");
        $(".sgvl-info-menu-drop").dropdown("set text", item.text());
    }

    $(".sgvl-navigation-left").find(".ui.dropdown").dropdown();

    SGVL.dropdown = function (element) {
        $(element).dropdown({
            forceSelection: false
        });
    };

    SGVL.inputdatepicker = function (element) {
        var $this = $(element);
        $this.datepicker({
            format: "dd/mm/yyyy",
            days: ["D", "S", "T", "Q", "Q", "S", "S"],
            daysShort: ["D", "S", "T", "Q", "Q", "S", "S"],
            daysMin: ["D", "S", "T", "Q", "Q", "S", "S"],
            months: ["JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"],
            monthsShort: ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"],
            autoHide: true
        }).on("change", () => {
            var $form = $this.closest("form");
            if ($form) $form.form("validate field", $this.attr("name"));
        });
    };

    SGVL.inputdatepickerMes = function (element) {
        var $this = $(element);
        $this.datepicker({
            format: "mm",
            months: ["JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"],
            monthsShort: ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"],
            autoHide: true,
            startView: 2
        })
    };

    SGVL.inputdatepickerAno = function (element) {
        var $this = $(element);
        $this.datepicker({
            format: "yyyy",
            Type: Boolean,
            Default: false,
            startView: 2
        })
    };

    SGVL.OnpenMenu = function (e) {
        $(e).closest(".sgvl-navigation-left").toggleClass("ativo");
    };

    SGVL.OnpenSubMenu = function (e) {
        $(e).closest(".sgvl-container").find("article.sgvl-article").toggleClass("ativo");
        $(e).closest(".item").toggleClass("ativo");
    };

    SGVL.notification = function (type, message) {
        switch (type) {
            case "success":
                Lobibox.notify("success",
                    {
                        position: "top right",
                        sound: "sound2",
                        delay: 3000,
                        msg: message,
                        title: "Sucesso",
                        icon: "smile icon"
                    });
                break;
            case "error":
                Lobibox.notify("error",
                    {
                        position: "top right",
                        sound: "sound4",
                        delay: 3000,
                        msg: message,
                        title: "Erro",
                        icon: "frown icon"
                    });
                break;
            case "warning":
                Lobibox.notify("warning",
                    {
                        position: "top right",
                        sound: "sound5",
                        delay: 3000,
                        msg: message,
                        title: "Alerta",
                        icon: "warning icon"
                    });
                break;
            case "info":
                Lobibox.notify("info",
                    {
                        position: "top right",
                        sound: "sound6",
                        delay: 3000,
                        msg: message,
                        title: "Informação",
                        icon: "info circle icon"
                    });
                break;
            default:
        }
    }
});