﻿using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class QuadraRepository : RepositorioBase<Quadra>
    {
        public Quadra BuscarPorId(Guid id)
        {
            return DbSet
                .Include(q => q.Loteamento)
                .FirstOrDefault(c => c.QuadraId == id);
        }

        public IEnumerable<Quadra> ObterQuadraPorLoteamento(Guid loteamentoId)
        {
            return DbSet
                .Include(q => q.Loteamento)
                .Where(q => q.LoteamentoId == loteamentoId)
                .OrderBy(x => x.IdentificacaoDaQuadra);
        }

        public IEnumerable<Quadra> BuscarPorNome(string nome)
        {
            return DbSet
                .Include(x => x.Loteamento)
                .Where(x => x.IdentificacaoDaQuadra.StartsWith(nome) && x.Loteamento.Proprietario.Usuario.UsuarioId == Account.UsuarioId)
                .OrderBy(x => x.IdentificacaoDaQuadra)
                .OrderBy(x => x.Loteamento.Nome);
        }
    }
}