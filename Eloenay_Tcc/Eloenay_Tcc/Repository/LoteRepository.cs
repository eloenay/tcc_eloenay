﻿using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class LoteRepository : RepositorioBase<Lote>
    {
        public Lote BuscarPorId(Guid id)
        {
            return Context.Lotes
                .Include(l => l.Quadra)
                .Include(l => l.Quadra.Loteamento)
                .FirstOrDefault(c => c.LoteId == id);
        }

        public IEnumerable<Lote> ObterLotePorQuadra(Guid quadraId)
        {
            return DbSet
                .Include(l => l.Quadra)
                .Where(l => l.QuadraId == quadraId)
                .OrderBy(x => x.IdentificacaoDoLote);
        }

        public IEnumerable<Lote> ObterLotePeloLoteamento(Guid loteamentoId)
        {
            return DbSet
                .Include(l => l.Quadra)
                .Where(l => l.Quadra.LoteamentoId == loteamentoId)
                .OrderBy(x => x.IdentificacaoDoLote);
        }

        public IEnumerable<Lote> ObterLotesVendidosPeloLoteamento(Guid loteamentoId)
        {
            return DbSet
                .Include(l => l.Quadra)
                .Include(l => l.Vendas.Select(v => v.Parcelas))
                .Where(l => l.Quadra.LoteamentoId == loteamentoId && l.Vendas != null)
                .OrderBy(x => x.IdentificacaoDoLote);
        }

        public IEnumerable<Lote> BuscarPorNome(string nome)
        {
            return DbSet
                .Include(x => x.Quadra)
                .Where(x => x.IdentificacaoDoLote.Contains(nome) && x.Quadra.Loteamento.Proprietario.Usuario.UsuarioId == Account.UsuarioId)
                .OrderBy(x => x.IdentificacaoDoLote);
        }

        public IEnumerable<Lote> BuscarLote(Guid id)
        {
            return DbSet
                .Include(x => x.Quadra)
                .Where(x => x.LoteId == id)
                .OrderBy(x => x.IdentificacaoDoLote);
        }
    }
}