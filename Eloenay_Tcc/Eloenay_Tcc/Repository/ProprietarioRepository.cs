﻿using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class ProprietarioRepository : RepositorioBase<Proprietario>
    {
        public IEnumerable<Proprietario> BuscarTodos()
        {
            return DbSet.Where(x => x.UsuarioId == Account.UsuarioId).OrderBy(x => x.Nome);
        }

        public Proprietario BuscarPorId(Guid id)
        {
            return DbSet.FirstOrDefault(c => c.ProprietarioId == id);
        }

        public IEnumerable<Proprietario> BuscarPorNome(string nome)
        {
            return DbSet.Where(x => x.Nome.StartsWith(nome) && x.Usuario.UsuarioId == Account.UsuarioId)
                .OrderBy(x => x.Nome);
        }
    }
}