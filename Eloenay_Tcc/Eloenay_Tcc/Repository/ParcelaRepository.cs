﻿using Eloenay_Tcc.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class ParcelaRepository : RepositorioBase<Parcela>
    {
        public IEnumerable<Parcela> BuscarTodosPorVenda(Guid id)
        {
            return DbSet.Where(x => x.VendaId == id).OrderBy(x => x.IdentificacaoParcela);
        }

        public Parcela BuscarPorId(Guid id)
        {
            return DbSet.FirstOrDefault(c => c.ParcelaId == id);
        }

        public IEnumerable<Parcela> BuscarPorNome(string nome)
        {
            return DbSet
                .Include(x => x.Venda.Cliente)
                .Where(x => x.Venda.Cliente.Nome.StartsWith(nome))
                .OrderBy(x => x.Venda.Cliente.Nome);
        }

        public Parcela ObterValorParcela(Guid id)
        {
            return DbSet.FirstOrDefault(x => x.VendaId == id);
        }

        public IEnumerable<Parcela> BuscarTodosPorLoteamento(Guid id)
        {
            return DbSet
                .Include(x => x.Venda.Cliente)
                .Include(x => x.Venda.Lote.Quadra)
                .Where(x => x.Venda.Lote.Quadra.Loteamento.LoteamentoId == id);
        }

        public IEnumerable<Parcela> BuscarTodosPorLoteamentoPeriodo(Guid id, int ano, int mes)
        {
            return DbSet
                .Include(x => x.Venda.Cliente)
                .Include(x => x.Venda.Lote.Quadra)
                .Where(x => x.Venda.Lote.Quadra.Loteamento.LoteamentoId == id && x.DataDaParcela.Month == mes && x.DataDaParcela.Year == ano);
        }
    }
}