﻿using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class VendaRepository : RepositorioBase<Venda>
    {
        public override Venda Adicionar(Venda entity)
        {
            Context.Lotes.Attach(entity.Lote);
            return base.Adicionar(entity);
        }

        public Venda BuscarPorId(Guid id)
        {
            return Context.Vendas
                .Include(x => x.Cliente)
                .Include(x => x.Lote.Quadra.Loteamento.Proprietario)
                .Include(x => x.Parcelas)
                .FirstOrDefault(c => c.VendaId == id);
        }

        public IEnumerable<Venda> BuscarPorNome(string nome)
        {
            return DbSet
                .Include(x => x.Cliente)
                .Include(x => x.Lote.Quadra.Loteamento)
                .Include(x => x.Parcelas)
                .Where(x => x.Cliente.Nome.StartsWith(nome) && x.Usuario.UsuarioId == Account.UsuarioId)
                .OrderBy(x => x.DataDaCompra);
        }

        public IEnumerable<Venda> BuscarTodos()
        {
            return DbSet
                .Include(x => x.Cliente)
                .Include(x => x.Lote.Quadra.Loteamento)
                .Include(x => x.Parcelas)
                .Where(x => x.Usuario.UsuarioId == Account.UsuarioId && x.StatusDaVenda == false)
                .OrderBy(x => x.DataDoCancelamento);
        }

        public int QuantidadesDeVendasCanceladas(Guid id)
        {
            return DbSet.Where(x => x.StatusDaVenda == false && x.Lote.Quadra.Loteamento.LoteamentoId == id).Count();
        }

        public int QuantidadesDeVendasFinalizada(Guid id)
        {
            return DbSet.Where(x => x.StatusDaVenda == true && x.Parcelas.Count() == x.Parcelas.Count(s => s.StatusDaParcela == true) && x.Lote.Quadra.Loteamento.LoteamentoId == id).Count();
        }

        public IEnumerable<Venda> BuscarTodasPorLoteamento(Guid loteamentoId)
        {
            return DbSet.Include(x => x.Lote.Quadra).Include(x => x.Parcelas).Where(x => x.Lote.Quadra.LoteamentoId == loteamentoId);
        }
    }
}