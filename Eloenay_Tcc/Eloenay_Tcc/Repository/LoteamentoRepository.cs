﻿using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class LoteamentoRepository : RepositorioBase<Loteamento>
    {
        public IEnumerable<Loteamento> BuscarTodos()
        {
            return DbSet.Where(x => x.Proprietario.UsuarioId == Account.UsuarioId).OrderBy(x => x.Nome);
        }

        public Loteamento BuscarPorId(Guid id)
        {
            return DbSet
                .Include(l => l.Proprietario)
                .Include(x => x.Quadras.Select(q => q.Lotes.Select(c => c.Vendas.Select(p => p.Parcelas))))
                .FirstOrDefault(c => c.LoteamentoId == id);
        }

        public IEnumerable<Loteamento> BuscarPorNome(string nome)
        {
            return DbSet
                .Include(x => x.Proprietario.Usuario)
                .Where(x => x.Nome.StartsWith(nome) && x.Proprietario.Usuario.UsuarioId == Account.UsuarioId)
                .OrderBy(x => x.Nome).OrderBy(x => x.Nome);
        }
    }
}