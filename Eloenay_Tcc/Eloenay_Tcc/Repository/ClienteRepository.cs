﻿using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class ClienteRepository : RepositorioBase<Cliente>
    {
        public Cliente BuscarPorId(Guid id)
        {
            return DbSet.FirstOrDefault(c => c.ClienteId == id);
        }

        public IEnumerable<Cliente> BuscarPorNome(string nome)
        {
            return DbSet.Where(p => p.Nome.StartsWith(nome) && p.UsuarioId == Account.UsuarioId).OrderBy(x => x.Nome);
        }
    }
}