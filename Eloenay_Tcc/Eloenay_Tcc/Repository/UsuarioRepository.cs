﻿using Eloenay_Tcc.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Eloenay_Tcc.Repository
{
    public class UsuarioRepository : RepositorioBase<Usuario>
    {
        public Usuario BuscarPorId(Guid id)
        {
            return Context.Usuarios.FirstOrDefault(c => c.UsuarioId == id);
        }

        public Usuario BuscarPorLogin(string login)
        {
            return DbSet.FirstOrDefault(c => c.Login == login);
        }

        public IEnumerable<Usuario> BuscarTodos()
        {
            return DbSet;
        }
    }
}