﻿using Eloenay_Tcc.Context;
using Eloenay_Tcc.Intefaces.Repositories;
using System;
using System.Data.Entity;

namespace Eloenay_Tcc.Repository
{
    public class RepositorioBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected ContextBanco Context;
        protected DbSet<TEntity> DbSet;

        public RepositorioBase()
        {
            Context = new ContextBanco();
            DbSet = Context.Set<TEntity>();
        }

        public virtual TEntity Adicionar(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Added;
            Context.SaveChanges();
            return entity;
        }

        public virtual TEntity Atualizar(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
            return entity;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Context.Dispose();
        }
    }
}