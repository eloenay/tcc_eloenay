﻿using Eloenay_Tcc.Helpers;
using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class VendaController : Controller
    {
        private readonly Venda _venda;
        private readonly Cliente _cliente;
        private readonly Loteamento _loteamento;
        private readonly Lote _lote;
        private readonly Quadra _quadra;
        private readonly Parcela _parcelas;

        public VendaController(Venda venda, Cliente cliente, Loteamento loteamento, Lote lote, Quadra quadra, Parcela parcelas)
        {
            _venda = venda;
            _cliente = cliente;
            _loteamento = loteamento;
            _lote = lote;
            _quadra = quadra;
            _parcelas = parcelas;
        }

        public ActionResult Index(string buscaFinalizada = "", int pagina = 1)
        {
            var vendas = _venda.BuscarPorNome(buscaFinalizada).Where(x => x.StatusDaVenda == true).ToPagedList(pagina, 7);
            ViewBag.Busca = buscaFinalizada;
            return View(vendas);
        }

        [Route("Venda/Index/Canceladas")]
        public ActionResult VendasCanceladas(string buscaCancelada = "", int pagina = 1)
        {
            var vendas = _venda.BuscarPorNome(buscaCancelada).Where(x => x.StatusDaVenda == false).ToPagedList(pagina, 7);
            ViewBag.BuscaCancelada = buscaCancelada;
            return View(vendas);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        [Route("Venda/Adicionar/{id}")]
        public ActionResult AdicionarVendaPelaHome(Guid id)
        {
            var lote = _lote.BuscarPorId(id);
            ViewBag.LoteId = lote.LoteId;
            ViewBag.LoteIdentificacao = lote.IdentificacaoDoLote;
            ViewBag.LoteQuadra = lote.Quadra.IdentificacaoDaQuadra;
            ViewBag.LoteValor = lote.ValorDoLote;
            return View("Adicionar");
        }

        public ActionResult Detalhes(Guid id)
        {
            ViewBag.ValorParcela = string.Format("{0:#,0.00}", _parcelas.ObterValorParcela(id).ValorDaParcela);
            var vendas = _venda.BuscarPorId(id);
            return View(vendas);
        }

        [HttpPost]
        public ActionResult Adicionar(Venda venda, decimal valorEntrada, int quantidadeDeParcelas, double juros, DateTime primeiraParcela)
        {
            ModelState.Remove(nameof(venda.Lote));
            var lote = _lote.BuscarPorId(venda.LoteId);
            venda.Lote = lote;
            venda.UsuarioId = Account.UsuarioId;
            _venda.AtribuirValores(venda);
            lote.AtualizarStatus();
            _lote.Atualizar(lote);
            _venda.Adicionar(venda);

            var valorParcelas = _parcelas.ValorDasParcelas(lote.ValorDoLote, valorEntrada, quantidadeDeParcelas, juros);
            _parcelas.CriarParcelas(venda.VendaId, primeiraParcela, quantidadeDeParcelas, valorParcelas);
            return RedirectToAction("Detalhes", new { id = venda.VendaId }).Success("Venda realizada com sucesso");
        }

        #region Json
        public ActionResult CancelarVenda(Guid id)
        {
            var cancelarVenda = _venda.BuscarPorId(id);
            var lote = _lote.BuscarPorId(cancelarVenda.Lote.LoteId);
            cancelarVenda.AtualizarStatus();
            lote.AtualizarStatus();
            _venda.Atualizar(cancelarVenda);
            _lote.Atualizar(lote);
            return Json(cancelarVenda.VendaId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AdicionarCliente(Cliente cliente)
        {
            if (!ModelState.IsValid) return View("Adicionar");
            cliente.UsuarioId = Account.UsuarioId;
            _cliente.Adicionar(cliente);
            return Json(cliente, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BuscarVendaParaCancelar(Guid id)
        {
            var venda = _venda.BuscarPorId(id);
            return PartialView("_ModalCancelarVendas", venda);
        }

        public JsonResult ObterClienteParaAutocomplete(string nome)
        {
            var clientes = _cliente.BuscarPorNome(nome).Select(x => new { label = x.Nome, id = x.ClienteId, cpf = x.Cpf, dataNasc = x.DataNascimento.ToShortDateString() }).ToList();
            return Json(clientes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObterLoteDaHomeParaVenda(Guid id)
        {
            var lotes = _lote.BuscarLote(id).Select(x => new { id = x.LoteId, name = x.IdentificacaoDoLote, quadra = x.Quadra.IdentificacaoDaQuadra, valor = x.ValorDoLote }).ToList();
            return Json(lotes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObterLoteamento()
        {
            var loteamento = _loteamento.BuscarTodos();
            return Json(loteamento.Select(x => $"<option value='{x.LoteamentoId}'>{x.Nome}</option>"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObterQuadraPorLoteamento(Guid loteamentoId)
        {
            var quadra = _quadra.ObterQuadraPorLoteamento(loteamentoId);
            return Json(quadra.Select(x => $"<option value='{x.QuadraId}'>{x.IdentificacaoDaQuadra}</option>"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObterLotePorQuadra(Guid quadraId)
        {
            var lote = _lote.ObterLotePorQuadra(quadraId).Where(x => x.StatusDoLote == true);
            return Json(lote.Select(x => $"<option value='{x.LoteId}'>{x.IdentificacaoDoLote}</option>"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ModalCadastrarClienteVenda()
        {
            return PartialView("_ModalCadastrarCliente");
        }

        public ActionResult ModalBuscarLoteVenda()
        {
            return PartialView("_ModalBuscarLote");
        }

        public JsonResult BuscarContratoDeVendas(Guid id)
        {
            var venda = _venda.BuscarPorId(id);
            var primeParcela = venda.Parcelas.OrderBy(x => x.DataDaParcela).FirstOrDefault();
            var ultiParcela = venda.Parcelas.OrderByDescending(x => x.DataDaParcela).FirstOrDefault();
            var valorDaParcela = venda.Parcelas.FirstOrDefault();
            var model = new
            {
                dataCompra = venda.DataCompraFormat,
                dataCancelamento = venda.DataCancelamentoFormat,
                multa = venda.MultaDeRescisao,
                valorEntrada = venda.ValorEntradaFormat,
                parcelasPagas = venda.ParcelaPaga,
                valorParcelas = valorDaParcela.ValorParcelaFormt,
                primeiraParcela = primeParcela.DataParcelaFormt,
                ultimaParcela = ultiParcela.DataParcelaFormt,
                valorDevolucao = venda.ValorDevolucao,
                valorDesconto = venda.ValorDescontado,
                //Dados do Proprietario para contrato
                nomeProp = venda.Lote.Quadra.Loteamento.Proprietario.Nome,
                nascionalidadeProp = venda.Lote.Quadra.Loteamento.Proprietario.Nacionalidade,
                estadoCivilProp = venda.Lote.Quadra.Loteamento.Proprietario.EstadoCivil,
                ocupacaoProp = venda.Lote.Quadra.Loteamento.Proprietario.Ocupacao,
                rgProp = venda.Lote.Quadra.Loteamento.Proprietario.Rg,
                orgaoExpedidorProp = venda.Lote.Quadra.Loteamento.Proprietario.OrgaoExpedidor,
                cpfProp = venda.Lote.Quadra.Loteamento.Proprietario.Cpf,
                enderecoProp = venda.Lote.Quadra.Loteamento.Proprietario.Endereco,
                numeroProp = venda.Lote.Quadra.Loteamento.Proprietario.Numero,
                bairroProp = venda.Lote.Quadra.Loteamento.Proprietario.Bairro,
                cidadeProp = venda.Lote.Quadra.Loteamento.Proprietario.Cidade,
                estadoProp = venda.Lote.Quadra.Loteamento.Proprietario.Estado,
                //Dados do Cliente para contrato
                nomeCliente = venda.Cliente.Nome,
                nascionalidadeCliente = venda.Cliente.Nacionalidade,
                estadoCivilCliente = venda.Cliente.EstadoCivil,
                ocupacaoCliente = venda.Cliente.Ocupacao,
                rgCliente = venda.Cliente.Rg,
                orgaoExpedidorCliente = venda.Cliente.OrgaoExpedidor,
                cpfCliente = venda.Cliente.Cpf,
                enderecoCliente = venda.Cliente.Endereco,
                numeroCliente = venda.Cliente.Numero,
                bairroCliente = venda.Cliente.Bairro,
                cidadeCliente = venda.Cliente.Cidade,
                estadoCliente = venda.Cliente.Estado,
                //Dados de Lote, Quadra e Loteamento
                identificacaoLote = venda.Lote.IdentificacaoDoLote,
                metrosQuadradosLote = venda.Lote.MetroQuadradoLote,
                valorLote = string.Format("{0:#,0.00}", venda.Lote.ValorDoLote),
                identificacaoQuadra = venda.Lote.Quadra.IdentificacaoDaQuadra,
                nomeLoteamento = venda.Lote.Quadra.Loteamento.Nome,
                matriculaLoteamento = venda.Lote.Quadra.Loteamento.Matricula,
                bairroLoteamento = venda.Lote.Quadra.Loteamento.Endereco,
                enderecoLoteamento = venda.Lote.Quadra.Loteamento.Bairro,
                cidadeLoteamento = venda.Lote.Quadra.Loteamento.Cidade,

            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}