﻿using Eloenay_Tcc.Helpers;
using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using PagedList;
using System;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class ClienteController : Controller
    {
        private readonly Cliente _cliente;

        public ClienteController(Cliente cliente)
        {
            _cliente = cliente;
        }

        public ActionResult Index(string busca = "", int pagina = 1)
        {
            var cliente = _cliente.BuscarPorNome(busca).ToPagedList(pagina, 6);
            ViewBag.Busca = busca;
            return View(cliente);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        public ActionResult Atualizar(Guid id)
        {
            var clientes = _cliente.BuscarPorId(id);
            return View(clientes);
        }

        public ActionResult Detalhes(Guid id)
        {
            var clientes = _cliente.BuscarPorId(id);
            return View(clientes);
        }

        [HttpPost]
        public ActionResult Adicionar(Cliente cliente)
        {
            if (!ModelState.IsValid) return View(cliente).Error("Erro ao cadastrar cliente");
            cliente.UsuarioId = Account.UsuarioId;
            _cliente.Adicionar(cliente);
            return RedirectToAction("Index").Success("Cliente cadastrado com sucesso");
        }

        [HttpPost]
        public ActionResult Atualizar(Cliente cliente)
        {
            if (!ModelState.IsValid) return View(cliente).Error("Erro ao atualizar cliente");
            cliente.UsuarioId = Account.UsuarioId;
            _cliente.Atualizar(cliente);
            return RedirectToAction("Index").Success("Cliente atualizado com sucesso");
        }
    }
}