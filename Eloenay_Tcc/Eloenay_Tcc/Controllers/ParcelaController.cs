﻿using Eloenay_Tcc.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class ParcelaController : Controller
    {
        readonly Parcela _parcelas;
        readonly Venda _venda;

        public ParcelaController(Parcela parcelas, Venda venda)
        {
            _parcelas = parcelas;
            _venda = venda;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Atualizar(Parcela parcelas)
        {
            if (!ModelState.IsValid) return View();
            var parcela = _parcelas.BuscarPorId(parcelas.ParcelaId);
            parcela.ValorPagoDaParcela = parcelas.ValorPagoDaParcela;
            parcela.AtualizarStatus(parcelas.StatusDaParcela);
            _parcelas.Atualizar(parcela);
            return Json(parcela.VendaId, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BuscarParcelasPorVenda(string nome)
        {
            var parcelas = _venda.BuscarPorNome(nome)
                                .Where(x => x.StatusDaVenda == true)
                                .Select(x => new
                                          {
                                             label = $"{x.Cliente.Nome} - " +
                                                     $"{x.Cliente.Cpf} - " +
                                                     $"{x.Lote.Quadra.Loteamento.Nome} - " +
                                                     $"{x.Lote.Quadra.IdentificacaoDaQuadra} - " +
                                                     $"{x.Lote.IdentificacaoDoLote}",
                                             id = x.VendaId
                                          }).ToList();
            return Json(parcelas, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PreencherTabela(Guid id)
        {
            var parcelas = _parcelas.BuscarTodosPorVenda(id);
            return PartialView("_TabelaParcelas", parcelas);
        }

        public ActionResult ObterModalParaAtualizarParcelas(Guid id)
        {
            var parcela = _parcelas.BuscarPorId(id);
            return PartialView("_ModalConfirmacao", parcela);
        }
    }
}