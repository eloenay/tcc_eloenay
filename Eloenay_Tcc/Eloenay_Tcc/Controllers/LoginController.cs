﻿using Eloenay_Tcc.Helpers;
using Eloenay_Tcc.Models;
using Eloenay_Tcc.Utils;
using Eloenay_Tcc.ViewModels;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    public class LoginController : Controller
    {
        private readonly Usuario _usuario;

        public LoginController(Usuario usuario)
        {
            _usuario = usuario;
        }

        [Authorize]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Cadastrar(CadastrarUsuarioVm usuario)
        {
            if (!ModelState.IsValid) return View(usuario).Error("Erro ao cadastrar usuário!");
            if (_usuario.BuscarTodos().Count(u => u.Login == usuario.Login) > 0) return View(usuario).Error("Esse login já está em uso");

            Usuario novoUsuario = new Usuario
            {
                Nome = usuario.Nome,
                Login = usuario.Login,
                Senha = Hash.GerarHash(usuario.Senha),
                TipoUsuario = TipoUsuario.Administrador
            };
            _usuario.Adicionar(novoUsuario);
            return View().Success("Usuário cadastrado com sucesso!");
        }

        public ActionResult Entrar(string ReturnUrl)
        {
            var usuario = new LoginVm
            {
                UrlRetorno = ReturnUrl
            };
            return View(usuario);
        }

        [HttpPost]
        public ActionResult Entrar(LoginVm loginVm)
        {
            if (!ModelState.IsValid) return View(loginVm).Error("Erro ao fazer login!");
            if (loginVm == null) return View(loginVm).Error("Preencha todos os campos!");
            var usuario = _usuario.BuscarPorLogin(loginVm.Login);
            if (usuario == null) return View(loginVm).Error("Este usuário não existe!");
            if (usuario.Senha != Hash.GerarHash(loginVm.Senha)) return View(loginVm).Error("Senha incorreta!");

            var identity = new ClaimsIdentity(new[]
            {
                new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider","ASP.NET Identity"),
                new Claim(ClaimTypes.NameIdentifier, usuario.UsuarioId.ToString()),
                new Claim(ClaimTypes.Name, usuario.Nome),
            }, "ApplicationCookie");
            Request.GetOwinContext().Authentication.SignIn(identity);

            if (!String.IsNullOrWhiteSpace(loginVm.UrlRetorno) || Url.IsLocalUrl(loginVm.UrlRetorno)) return Redirect(loginVm.UrlRetorno);
            return RedirectToAction("Index", "Home").Information($"Bem vindo {usuario.Nome}");
        }

        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");
            return RedirectToAction("Entrar", "Login");
        }

        [Authorize]
        public ActionResult AlterarSenha()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AlterarSenha(AlterarSenhaVm altrSenha)
        {
            if (!ModelState.IsValid) return View(altrSenha).Error("Erro ao alterar senha!");
            var identity = User.Identity as ClaimsIdentity;
            var login = identity.Claims.FirstOrDefault(c => c.Type == "Login").Value;
            var usuario = _usuario.BuscarPorLogin(login);
            if (Hash.GerarHash(altrSenha.SenhaAtual) != usuario.Senha) return View().Error("Senha incorreta!");
            usuario.Senha = Hash.GerarHash(altrSenha.NovaSenha);
            _usuario.Atualizar(usuario);
            return View().Success("Senha Atualizada com sucesso!");
        }
    }
}