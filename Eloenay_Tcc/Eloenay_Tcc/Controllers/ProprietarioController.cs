﻿using Eloenay_Tcc.Helpers;
using Eloenay_Tcc.Models;
using Eloenay_Tcc.ViewModels;
using PagedList;
using System;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class ProprietarioController : Controller
    {
        public readonly Proprietario _proprietario;

        public ProprietarioController(Proprietario proprietario)
        {
            _proprietario = proprietario;
        }

        public ActionResult Index(string busca = "", int pagina = 1)
        {
            var proprietarios = _proprietario.BuscarPorNome(busca).ToPagedList(pagina, 7);
            ViewBag.Busca = busca;
            return View(proprietarios);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        public ActionResult Atualizar(Guid id)
        {
            var proprietarios = _proprietario.BuscarPorId(id);
            return View(proprietarios);
        }

        public ActionResult Detalhes(Guid id)
        {
            var proprietarios = _proprietario.BuscarPorId(id);
            return View(proprietarios);
        }

        [HttpPost]
        public ActionResult Adicionar(Proprietario proprietario)
        {
            if (!ModelState.IsValid) return View(proprietario).Error("Error ao cadastrar proprietário");
            proprietario.UsuarioId = Account.UsuarioId;
            _proprietario.Adicionar(proprietario);
            return RedirectToAction("Index").Success("Proprietário cadastrado com sucesso");
        }

        [HttpPost]
        public ActionResult Atualizar(Proprietario proprietario)
        {
            if (!ModelState.IsValid) return View(proprietario).Error("Error ao atualizar proprietário");
            proprietario.UsuarioId = Account.UsuarioId;
            _proprietario.Atualizar(proprietario);
            return RedirectToAction("Index").Success("Proprietário atualizado com sucesso");
        }
    }
}