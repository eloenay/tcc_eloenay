﻿using Eloenay_Tcc.Helpers;
using Eloenay_Tcc.Models;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class QuadraController : Controller
    {
        private readonly Quadra _quadra;
        private readonly Loteamento _loteamento;
        public QuadraController(Quadra quadra, Loteamento loteamento)
        {
            _quadra = quadra;
            _loteamento = loteamento;
        }

        public ActionResult Index(string busca = "", int pagina = 1)
        {
            var quadra = _quadra.BuscarPorNome(busca).ToPagedList(pagina, 7);
            ViewBag.Busca = busca;
            return View(quadra);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        public ActionResult Atualizar(Guid id)
        {
            var quadra = _quadra.BuscarPorId(id);
            return View(quadra);
        }

        public ActionResult Detalhes(Guid id)
        {
            var quadra = _quadra.BuscarPorId(id);
            return View(quadra);
        }

        [HttpPost]
        public ActionResult Adicionar(Quadra quadra, double largura, double comprimento)
        {
            if (!ModelState.IsValid) return View(quadra).Error("Erro ao cadastrar quadra");
            quadra.CalcularMetrosQuadrados(largura, comprimento);
            _quadra.Adicionar(quadra);
            return RedirectToAction("Index").Success("Quadra cadastrada com sucesso");
        }

        [HttpPost]
        public ActionResult Atualizar(Quadra quadra, double largura, double comprimento)
        {
            if (!ModelState.IsValid) return View(quadra).Error("Erro ao atualizar quadra");
            quadra.CalcularMetrosQuadrados(largura, comprimento);
            _quadra.Atualizar(quadra);
            return RedirectToAction("Index").Success("Quadra atualizada com sucesso");
        }

        public JsonResult ObterLoteamento()
        {
            var loteamento = _loteamento.BuscarTodos();
            return Json(loteamento.Select(x => $"<option value='{x.LoteamentoId}'>{x.Nome}</option>"), JsonRequestBehavior.AllowGet);
        }
    }
}