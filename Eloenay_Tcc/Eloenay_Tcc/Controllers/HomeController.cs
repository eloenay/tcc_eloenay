﻿using Eloenay_Tcc.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public readonly Loteamento _loteamento;
        public readonly Venda _venda;
        public readonly Parcela _parcelas;
        public readonly Lote _lotes;

        public HomeController(Loteamento loteamento, Venda venda, Parcela parcelas, Lote lote)
        {
            _loteamento = loteamento;
            _venda = venda;
            _parcelas = parcelas;
            _lotes = lote;
        }

        public ActionResult Index(string busca = "", int pagina = 1)
        {
            var loteamentos = _loteamento.BuscarPorNome(busca).ToPagedList(pagina, 7);
            ViewBag.Busca = busca;
            return View(loteamentos);
        }

        public ActionResult Detalhes(Guid loteamentoId)
        {
            var loteamento = _loteamento.BuscarPorId(loteamentoId);
            var vendas = _venda.BuscarTodasPorLoteamento(loteamentoId);
            ViewBag.QntVendaCancelada = _venda.QuantidadesDeVendasCanceladas(loteamentoId);
            ViewBag.QntVendaFinalizada = _venda.QuantidadesDeVendasFinalizada(loteamentoId);
            InformacoesEmParcelasParaViewBags(loteamentoId);
            InformacoesEmLoteParaViewBags(loteamentoId, vendas);
            return View(loteamento);
        }

        public ActionResult ObterParcelaPorPeriodo(Guid id, int ano, int mes)
        {
            var parcelas = _parcelas.BuscarTodosPorLoteamentoPeriodo(id, ano, mes);
            ViewBag.DataMes = mes;
            return PartialView("_TabelaInformacoesParcelas", parcelas);
        }

        public void InformacoesEmParcelasParaViewBags(Guid loteamentoId)
        {
            var parcelas = _parcelas.BuscarTodosPorLoteamento(loteamentoId);
            ViewBag.Parcelas = parcelas;
            ViewBag.ContagemDeTodasAsParcelas = _parcelas.ContagemDeTodasAsParcelas(parcelas);
            ViewBag.ContagemDeParcelasPagas = _parcelas.ContagemDeParcelasPagas(parcelas);
            ViewBag.ContagemDeParcelasPendentes = _parcelas.ContagemDeParcelasPendentes(parcelas);
            ViewBag.ContagemDeParcelasAtrasadas = _parcelas.ContagemDeParcelasAtrasadas(parcelas);
            ViewBag.ContagemDeTodasAsParcelasEmVendasCanceladas = _parcelas.ContagemDeTodasAsParcelasEmVendasCanceladas(parcelas);
            ViewBag.ContagemDeParcelasPagasEmVendasCanceladas = _parcelas.ContagemDeParcelasPagasEmVendasCanceladas(parcelas);
            ViewBag.ObterValorTotalDeParcelas = string.Format("{0:0,0.00}", _parcelas.ObterValorTotalDeParcelas(parcelas));
            ViewBag.ObterValorDasParcelasPagas = string.Format("{0:0,0.00}", _parcelas.ObterValorDasParcelasPagas(parcelas));
            ViewBag.ObterValorDasParcelasPendentes = string.Format("{0:0,0.00}", _parcelas.ObterValorDasParcelasPendentes(parcelas));
            ViewBag.ObterValorDasParcelasAtrasadas = string.Format("{0:0,0.00}", _parcelas.ObterValorDasParcelasAtrasadas(parcelas));
            ViewBag.ObterValorTotalDeParcelasEmVendasCanceladas = string.Format("{0:0,0.00}", _parcelas.ObterValorTotalDeParcelasEmVendasCanceladas(parcelas));
            ViewBag.ObterValorDasParcelasPagasEmVendasCanceladas = string.Format("{0:0,0.00}", _parcelas.ObterValorDasParcelasPagasEmVendasCanceladas(parcelas));
            ViewBag.TotalDeValorRestituido = string.Format("{0:0,0.00}", _parcelas.TotalDeValorRestituido(parcelas));
        }

        public void InformacoesEmLoteParaViewBags(Guid loteamentoId, IEnumerable<Venda> venda)
        {
            var lotes = _lotes.ObterLotePeloLoteamento(loteamentoId);
            var lotesVendidos = _lotes.ObterLotesVendidosPeloLoteamento(loteamentoId);
            ViewBag.QuantidadesDeLotes = _lotes.QuantidadesDeLotes(lotes);
            ViewBag.ValorTotalDosLotes = string.Format("{0:0,0.00}", _lotes.ValorTotalDosLotes(lotes));
            ViewBag.ValorTotalDosLotesDisponiveis = string.Format("{0:0,0.00}", _lotes.ValorTotalDosLotesDisponiveis(lotes));
            ViewBag.ValorTotalDosLotesEmVendasRealizadas = string.Format("{0:0,0.00}", _lotes.ValorTotalDosLotesEmVendasRealizadas(venda));
            ViewBag.ValorTotalDosLotesEmVendasFinalizadas = string.Format("{0:0,0.00}", _lotes.ValorTotalDosLotesEmVendasFinalizadas(venda));
            ViewBag.ValorTotalDosLotesDasVendasCanceladas = string.Format("{0:0,0.00}", _lotes.ValorTotalDosLotesDasVendasCanceladas(venda));
        }

        public ActionResult InformacoesLote(Guid loteId)
        {
            var lote = _lotes.BuscarPorId(loteId);
            return PartialView("_InformacoesDoLote", lote);
        }
    }
}