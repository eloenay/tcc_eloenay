﻿using Eloenay_Tcc.Helpers;
using Eloenay_Tcc.Models;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class LoteamentoController : Controller
    {
        public readonly Loteamento _loteamento;
        public readonly Proprietario _proprietario;

        public LoteamentoController(Loteamento loteamento, Proprietario proprietario)
        {
            _loteamento = loteamento;
            _proprietario = proprietario;
        }

        public ActionResult Index(string busca = "", int pagina = 1)
        {
            var loteamentos = _loteamento.BuscarPorNome(busca).ToPagedList(pagina, 7);
            ViewBag.Busca = busca;
            return View(loteamentos);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        public ActionResult Atualizar(Guid id)
        {
            var loteamentos = _loteamento.BuscarPorId(id);
            return View(loteamentos);
        }

        public ActionResult Detalhes(Guid id)
        {
            var loteamentos = _loteamento.BuscarPorId(id);
            return View(loteamentos);
        }

        [HttpPost]
        public ActionResult Adicionar(Loteamento loteamento)
        {
            if (!ModelState.IsValid) return View(loteamento).Error("Erro ao cadastrar loteamento");
            _loteamento.Adicionar(loteamento);
            return RedirectToAction("Index").Success("Loteamento cadastrado com sucesso");
        }

        [HttpPost]
        public ActionResult Atualizar(Loteamento loteamento)
        {
            if (!ModelState.IsValid) return View(loteamento).Error("Erro ao atualizar loteamento");
            _loteamento.Atualizar(loteamento);
            return RedirectToAction("Index").Success("Loteamento atualizado com sucesso");
        }

        public JsonResult ObterProprietario()
        {
            var proprietario = _proprietario.BuscarTodos();
            return Json(proprietario.Select(x => $"<option value='{x.ProprietarioId}'>{x.Nome}</option>"), JsonRequestBehavior.AllowGet);
        }
    }
}