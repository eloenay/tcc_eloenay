﻿using Eloenay_Tcc.Helpers;
using Eloenay_Tcc.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Eloenay_Tcc.Controllers
{
    [Authorize]
    public class LoteController : Controller
    {
        private readonly Lote _lote;
        private readonly Quadra _quadra;
        private readonly Loteamento _loteamento;

        public LoteController(Lote lote, Quadra quadra, Loteamento loteamento)
        {
            _lote = lote;
            _quadra = quadra;
            _loteamento = loteamento;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        public ActionResult Atualizar(Guid id)
        {
            var lote = _lote.BuscarPorId(id);
            return View(lote);
        }

        public ActionResult Detalhes(Guid id)
        {
            var lote = _lote.BuscarPorId(id);
            return View(lote);
        }

        [HttpPost]
        public ActionResult Adicionar(Lote lote, double largura, double comprimento)
        {
            if (!ModelState.IsValid) return View(lote).Error("Erro ao cadastrar lote");
            lote.CalcularMetrosQuadrados(largura, comprimento);
            _lote.Adicionar(lote);
            return RedirectToAction("Index").Success("Lote cadastrado com sucesso");
        }

        [HttpPost]
        public ActionResult Atualizar(Lote lote, double largura, double comprimento)
        {
            if (!ModelState.IsValid) return View(lote).Error("Erro ao atualizar lote");
            lote.CalcularMetrosQuadrados(largura, comprimento);
            var loteAtual = _lote.BuscarPorId(lote.LoteId);
            loteAtual.AtualizarDados(lote);
            _lote.Atualizar(loteAtual);
            return RedirectToAction("Index").Success("Lote atualizado com sucesso");
        }

        public ActionResult PreencherTabela(Guid id)
        {
            var lotesPorQuadra = _lote.ObterLotePorQuadra(id);
            return PartialView("_TabelaLote", lotesPorQuadra);
        }

        public JsonResult ObterLoteamento()
        {
            var loteamento = _loteamento.BuscarTodos();
            return Json(loteamento.Select(x => $"<option value='{x.LoteamentoId}'>{x.Nome}</option>"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObterQuadraPorLoteamento(Guid loteamentoId)
        {
            var quadra = _quadra.ObterQuadraPorLoteamento(loteamentoId);
            return Json(quadra.Select(x => $"<option value='{x.QuadraId}'>{x.IdentificacaoDaQuadra}</option>"), JsonRequestBehavior.AllowGet);
        }
    }
}