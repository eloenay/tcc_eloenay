﻿$(function () {

    "use script"

    window.Venda = window.Venda || {};

    Venda.Apoio = {
        CarregarEvento: () => {
            Number.prototype.formatMoney = function (c, d, t) {
                var n = this,
                    c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };

            $(document).on("click", ".ui.mini.button.btnCancelarVenda", function (e) {
                Venda.Buttons.BuscarVenda(e);
            });

            $(document).on("click", ".ui.negative.button.btnRetornarCancelarVenda", function () {
                $(".ui.tiny.modal.modalCancelarVenda").modal("hide");
            });

            $(document).on("click", ".ui.positive.right.button.btnSimVendaCancelada", function (e) {
                Venda.Buttons.CancelarVenda(e);
            });
        },
    };

    Venda.Buttons = {
        BuscarVenda: (e) => {
            var urlbusca = e.currentTarget.dataset.busca;
            $.ajax({
                url: urlbusca,
                method: "GET",
                success: function (data) {
                    $(".ui.tiny.modal.modalCancelarVenda").html(data);
                    $(".ui.tiny.modal.modalCancelarVenda").modal("show");
                },
            });
        },

        CancelarVenda: (e) => {
            var vendaId = e.currentTarget.dataset.contrato;
            var btn = $(".ui.positive.right.button.btnSimVendaCancelada").addClass("loading");
            var url = e.currentTarget.dataset.url;
            var urlRedirect = e.currentTarget.dataset.redirect;
            $.ajax({
                url: url,
                method: "post",
                success: function () {
                    ImprimirContrato.VendaCancelada(vendaId, btn);
                    $("body").empty();
                    $("body").load(urlRedirect);
                    SGVL.notification("success", "Venda cancelada com sucesso");
                    $(".ui.tiny.modal.modalCancelarVenda").modal("hide");
                },
                erro: function () {
                    SGVL.notification("erro", "Erro ao cancelar venda");
                    $(".ui.tiny.modal.modalCancelarVenda").modal("hide");
                },
            });
        }
    };

    Venda.Apoio.CarregarEvento();
});