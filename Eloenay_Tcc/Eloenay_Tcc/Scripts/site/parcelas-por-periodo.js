﻿$(function () {

    "use string"

    window.ParcelaPorPeriodo = window.ParcelaPorPeriodo || {};

    ParcelaPorPeriodo.Apoio = function () {

        $(document).on("change", ".calendarioMes", function (e) {
            $(".ui.basic.segment.sgvl-article").addClass("loading");
            var dataMes = $(".calendarioMes").val();
            var dataAno = $(".calendarioAno").val();
            var Ano = $("#Ano").val();
            var Mes = $("#Mes").val();
            var loteamentoId = $("#LoteamentoId").val();
            var url = $(".ui.form").data("url");

            if (dataAno !== "") {
                $.ajax({
                    url: url,
                    data: { id: loteamentoId, ano: dataAno, mes: dataMes },
                    success: function (result) {
                        $(".informacao-tabela-parcela").empty();
                        $(".informacao-tabela-parcela").html(result);
                        $(".ui.basic.segment.sgvl-article").removeClass("loading");
                    }
                });
            }
            else {
                $.ajax({
                    url: url,
                    data: { id: loteamentoId, ano: Ano, mes: dataMes },
                    success: function (result) {
                        $(".informacao-tabela-parcela").empty();
                        $(".informacao-tabela-parcela").html(result);
                        $(".ui.basic.segment.sgvl-article").removeClass("loading");
                    }
                });
            }
        });

        $(document).on("change", ".calendarioAno", function (e) {
            $(".ui.basic.segment.sgvl-article").addClass("loading");
            var dataMes = $(".calendarioMes").val();
            var dataAno = $(".calendarioAno").val();
            var Ano = $("#Ano").val();
            var Mes = $("#Mes").val();
            var loteamentoId = $("#LoteamentoId").val();
            var url = $(".ui.form").data("url");

            if (dataMes !== "") {
                $.ajax({
                    url: url,
                    data: { id: loteamentoId, ano: dataAno, mes: dataMes },
                    success: function (result) {
                        $(".informacao-tabela-parcela").empty();
                        $(".informacao-tabela-parcela").html(result);
                        $(".ui.basic.segment.sgvl-article").removeClass("loading");
                    }
                });
            }
            else {
                $.ajax({
                    url: url,
                    data: { id: loteamentoId, ano: dataAno, mes: Mes },
                    success: function (result) {
                        $(".informacao-tabela-parcela").empty();
                        $(".informacao-tabela-parcela").html(result);
                        $(".ui.basic.segment.sgvl-article").removeClass("loading");
                    }
                });
            }
        });
    };

    ParcelaPorPeriodo.Apoio();
});