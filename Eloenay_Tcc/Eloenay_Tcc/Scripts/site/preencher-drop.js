﻿$(function () {

    "use string"

    window.PreencherDrop = window.PreencherDrop || {};

    PreencherDrop.on = {

        init: () => {
            var pds = $(document.body).find("[data-pd]");
            $.each(pds, (i, item) => {
                var $item = $(item);
                var $pd = $item.data("pd");
                if (!$.isFunction(PreencherDrop.on[$pd])) return;
                PreencherDrop.on[$pd]($item);
            });
            sessionStorage.clear();
        },

        //Modal de lote Venda
        lt: ($this) => {

            var $loteamento = $this.find("[data-pd-lt='loteamento']");
            var $sltLoteamento = $loteamento.find("select")
            var $quadra = $this.find("[data-pd-lt='quadra']");
            var $sltQuadra = $quadra.find("select")
            var $lote = $this.find("[data-pd-lt='lote']");
            var $sltLote = $lote.find("select")

            $sltLoteamento.dropdown({
                onChange: (loteamentoId) => {
                    var $paramrs = { loteamentoId };
                    $sltQuadra.data("paramrs", $paramrs);

                    PreencherDrop.on.loadSelectOption($sltQuadra, () => PreencherDrop.on.setSelectOptionThree($sltQuadra, true), PreencherDrop.on.setClearSelect($sltQuadra));
                    PreencherDrop.on.setClearSelect($sltLote);
                }
            });
            PreencherDrop.on.loadSelectOption($sltLoteamento, () => PreencherDrop.on.setSelectOptionThree($sltLoteamento));

            $sltQuadra.dropdown({
                onChange: (quadraId) => {
                    var $paramrs = { quadraId };
                    $sltLote.data("paramrs", $paramrs);

                    PreencherDrop.on.loadSelectOption($sltLote, () => PreencherDrop.on.setSelectOptionThree($sltLote, true), PreencherDrop.on.setClearSelect($sltLote));
                    PreencherDrop.on.setClearSelect($sltLote);
                }
            });
        },

        //Form Lote
        lo: ($this) => {

            var $loteamento = $this.find("[data-pd-lo='loteamento']");
            var $sltLoteamento = $loteamento.find("select")
            var $quadra = $this.find("[data-pd-lo='quadra']");
            var $sltQuadra = $quadra.find("select")

            $sltLoteamento.dropdown({
                onChange: (loteamentoId) => {
                    var $paramrs = { loteamentoId };
                    $sltQuadra.data("paramrs", $paramrs);

                    PreencherDrop.on.loadSelectOption($sltQuadra);
                }
            });
            PreencherDrop.on.loadSelectOption($sltLoteamento, () => PreencherDrop.on.setSelectOptionTwo($sltLoteamento), PreencherDrop.on.setClearSelect($sltQuadra));
        },

        //Form Loteamento
        pr: ($this) => {

            var $proprietario = $this.find("[data-pd-pr='proprietario']");
            var $sltProprietario = $proprietario.find("select")

            PreencherDrop.on.loadSelectOption($sltProprietario, () => PreencherDrop.on.setSelectOptionTwo($sltProprietario));
        },


        //Form Quadra
        lm: ($this) => {

            var $loteamento = $this.find("[data-pd-lm='loteamento']");
            var $sltLoteamento = $loteamento.find("select")

            PreencherDrop.on.loadSelectOption($sltLoteamento, () => PreencherDrop.on.setSelectOptionTwo($sltLoteamento));
        },

        //Funções
        loadSelectOption: ($select, result) => {
            $select.find("option:not(:first)").remove();
            $select.closest(".ui.dropdown").dropdown("clear").addClass("loading");
            $.getJSON($select.data("pdUrl"), $select.data("paramrs")).then(options => {
                $select.append(options);
                $select.closest(".disabled").removeClass("disabled");
                $select.closest(".ui.dropdown").removeClass("loading");
            }).then(result);
        },

        setSelectOptionTwo: ($select, show) => {
            if (show) $select.closest(".ui.dropdown").dropdown("show");
            $select.closest(".ui.dropdown").dropdown("set selected", $select.data("pdSelected"));
        },

        setSelectOptionThree: ($select) => {
            $select.closest(".ui.dropdown").dropdown("set selected", $select.data("pdSelected"));
        },

        setClearSelect: ($select) => {
            $select.closest(".ui.dropdown").dropdown("clear").removeClass("loading");
        },
    };

    PreencherDrop.on.init();
});