﻿$(function () {

    "use script"

    window.AdicionarVenda = window.AdicionarVenda || {};

    Number.prototype.formatMedida = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 20 ? j % 20 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{20})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    AdicionarVenda.Apoio = {

        CarregarEvento: () => {
            $(document).on("click", ".buscarLote", function () {
                var modal = $(".ui.modal.add-lote");
                modal.modal("show");
            });

            $(document).on("change", "#QuantidadeDeParcelas", function () {
                var quantParcela = $("#QuantidadeDeParcelas").val();
                if (quantParcela <= 0) {
                    $("#QuantidadeDeParcelas").val("");
                    $(".msg-quantp").html("<b>Obrigatório pelo menos 1 parcela.</b>");
                } else {
                    $(".msg-quantp").empty();
                }
            });

            $(document).on("click", ".cadastrar-cliente", function () {
                var modal = $(".ui.modal.add-venda");
                $.ajax({
                    url: modal.data("cliente"),
                    method: "GET",
                    success: function (data) {
                        modal.empty();
                        modal.html(data);
                        modal.modal("show");
                    }
                });
            });

            $("#fechaModalLote").click(function () {
                $(".ui.modal.add-lote div.text").empty();
                $(".ui.modal.add-lote").modal("hide");
            });

            $(document).on("click", "#btnRetornarClienteVenda", function () {
                $(".ui.modal.add-venda").modal("hide");
            });

            $(document).on("click", "#btnCadastrarClienteVenda", function () {
                $("article.adicionarVenda").addClass("loading");
                AdicionarVenda.Buscar.CadastrarCliente();
            });

            $(document).on("keyup", "#MultaDeRescisao", function () {
                var multa = $("#MultaDeRescisao").val();
                if (multa < 0) {
                    $("#MultaDeRescisao").val("");
                } else if (multa > 25) {
                    $(".msg-multa").html("<div class='ui orange message'><b>Multa de rescisão de contrato está de 10 a 25% aplicados pela justiça atualmente. Valores acima disso é tido como abusivos.</b></div>");
                } else {
                    $(".msg-multa").empty();
                }
            });
        }
    };

    AdicionarVenda.Buscar = {
        Cliente: () => {
            $("#Cliente_Nome").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: $("#cliente").data("url"),
                        data: { nome: request.term },
                        dataType: "json",
                        success: function (data) {
                            response(data);
                        },
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    $("input[name=Cliente_Nome]").val(ui.item.label);
                    $("#Cliente_Cpf").val(ui.item.cpf);
                    $("#Cliente_DataNascimento").val(ui.item.dataNasc);
                    $("#ClienteId").val(ui.item.id);
                },
            });
        },

        Lote: () => {
            $(document).on("change", "select#Lote_LoteId", function () {
                var lote = $("select#Lote_LoteId").val();
                $.ajax({
                    url: $(".ui.form.lote").data("url"),
                    data: { id: lote },
                    dataType: "json",
                    success: function (data) {
                        $("#lote_nome").empty();
                        $("#quadra_nome").empty();
                        $("#valorDoLote").empty();
                        $("#LoteId").empty();
                        $.each(data, function (i, lote) {
                            $("#lote_nome").val(lote.name);
                            $("#quadra_nome").val(lote.quadra);
                            $("#valorDoLote").val(lote.valor.formatMedida(2, ',', '.'));
                            $("#LoteId").val(lote.id);
                        });
                    },
                });
            });
        },

        CadastrarCliente: () => {
            var url = $("#venda-cliente").attr("action");
            var form = $("form#venda-cliente").serialize();
            $.ajax({
                url: url,
                data: form,
                method: "post",
                success: function (data) {
                    $("input[name=Cliente_Nome]").val(data.Nome);
                    $("#Cliente_Cpf").val(data.Cpf);
                    $("#Cliente_DataNascimento").val(data.DataNascimentoFormt);
                    $("#ClienteId").val(data.ClienteId);
                    $("article.adicionarVenda").removeClass("loading");
                    $(".ui.modal.add-venda").empty();
                    $(".ui.modal.add-venda").modal("hide");
                },
                erro: function () {
                    $(".ui.modal.add-venda").removeClass("loading");
                }
            });
        }
    };

    AdicionarVenda.Buscar.Cliente();
    AdicionarVenda.Buscar.Lote();
    AdicionarVenda.Apoio.CarregarEvento();
});