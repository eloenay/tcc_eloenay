﻿$(function () {

    "use string"

    window.ValidarCampo = window.ValidarCampo || {};

    $(document).ready(function () {
        $('.ui.form')
            .form({
                fields: {
                    nome: {
                        identifier: 'Nome',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O nome é obrigatório!'
                            }
                        ]
                    },
                    clienteNome: {
                        identifier: 'Cliente_Nome',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O Cliente é obrigatório!'
                            }
                        ]
                    },
                    lote: {
                        identifier: 'Lote',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O Lote é obrigatório!'
                            }
                        ]
                    },
                    cpf: {
                        identifier: 'Cpf',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O cpf é obrigatório!'
                            }
                        ]
                    },
                    rg: {
                        identifier: 'Rg',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O rg é obrigatório!'
                            }
                        ]
                    },
                    orgaoExpedidor: {
                        identifier: 'OrgaoExpedidor',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O orgão expedidor é obrigatório!'
                            }
                        ]
                    },
                    nascionalidade: {
                        identifier: 'Nacionalidade',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'A nascionalidade é obrigatório!'
                            }
                        ]
                    },
                    telefone: {
                        identifier: 'Telefone',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O telefone é obrigatório!'
                            }
                        ]
                    },
                    ocupacao: {
                        identifier: 'Ocupacao',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'A ocupação é obrigatório!'
                            }
                        ]
                    },
                    endereco: {
                        identifier: 'Endereco',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O endereco é obrigatório!'
                            }
                        ]
                    },
                    numero: {
                        identifier: 'Numero',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O número é obrigatório!'
                            }
                        ]
                    },
                    bairro: {
                        identifier: 'Bairro',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O bairro é obrigatório!'
                            }
                        ]
                    },
                    cep: {
                        identifier: 'Cep',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O cep é obrigatório!'
                            }
                        ]
                    },
                    cidade: {
                        identifier: 'Cidade',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'A cidade é obrigatório!'
                            }
                        ]
                    },
                    estado: {
                        identifier: 'Estado',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O estado é obrigatório!'
                            }
                        ]
                    },
                    valorLote: {
                        identifier: 'ValorDoLote',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O valor do lote é obrigatório!'
                            }
                        ]
                    },
                    matricula: {
                        identifier: 'Matricula',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'A matricula é obrigatório!'
                            }
                        ]
                    },
                    identificacaoQuadra: {
                        identifier: 'IdentificacaoDaQuadra',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'A identificação da quadra é obrigatório!'
                            }
                        ]
                    },
                    identificacaoLote: {
                        identifier: 'IdentificacaoDoLote',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'A identificação do lote é obrigatório!'
                            }
                        ]
                    },
                    metrosQuadrados: {
                        identifier: 'MetrosQuadrados',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Os metros quadrados é obrigatório!'
                            }
                        ]
                    },
                    metrosQuadradosQuadra: {
                        identifier: 'MetroQuadradoDaQuadra',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Os metros quadrados é obrigatório!'
                            }
                        ]
                    },
                    metrosQuadradosLote: {
                        identifier: 'MetroQuadradoLote',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Os metros quadrados é obrigatório!'
                            }
                        ]
                    },
                    largura: {
                        identifier: 'Largura',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'A largura é obrigatório!'
                            }
                        ]
                    },
                    comprimento: {
                        identifier: 'Comprimento',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O comprimento é obrigatório!'
                            }
                        ]
                    },
                    valorEntrada: {
                        identifier: 'ValorEntrada',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Defina um valor de entrada!'
                            }
                        ]
                    },
                    juros: {
                        identifier: 'Juros',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Defina um valor para o juros!'
                            }
                        ]
                    },
                    multa: {
                        identifier: 'MultaDeRescisao',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Defina um valor para a multa!'
                            }
                        ]
                    },
                    quantidadeParcelas: {
                        identifier: 'QuantidadeDeParcelas',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Defina a quantidade de parcelas!'
                            }
                        ]
                    },
                    login: {
                        identifier: 'Login',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Preencha o campo do usuário!'
                            }
                        ]
                    },
                    senha: {
                        identifier: 'Senha',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Preencha o campo da senha!'
                            }
                        ]
                    },
                },
                inline: true,
                on: 'blur',
            });
    });
});