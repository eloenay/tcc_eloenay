﻿$(function () {

    "use script"

    window.Parcelas = window.Parcelas || {};

    Parcelas.Apoio = {
        CarregarEvento: () => {
            $(document).on("click", ".statusBtn", function (e) {
                var url = e.currentTarget.dataset.url;
                var modalConfirmacao = $(".ui.tiny.modal.modalStatus");
                $.ajax({
                    url: url,
                    method: "GET",
                    success: function (result) {
                        modalConfirmacao.html(result);
                        modalConfirmacao.modal("show");
                        $(".ValorPagoParcela").val("");
                    }
                });
            });

            $(document).on("click", ".btnSimStatus", function () {
                Parcelas.Crud.Atualizar();
            });
        }
    };

    Parcelas.Buscar = {
        ParcelasPorVenda: () => {
            $("#Cliente_Nome").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: $("#parcelas").data("url"),
                        data: { nome: request.term },
                        dataType: "json",
                        success: function (data) {
                            if (data !== null) {
                                response(data);
                            } else {
                                response("Informação não encontrada")
                            }
                        },
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    $("#VendaId").val(ui.item.id);
                },
            });
        },

        TabelaParcelas: () => {
            $("#btnPesquisarParcelas").click(function () {
                var $form = $(this);
                $form.addClass("loading");
                var venda = $("#VendaId").val();
                var url = $("#tabelaParcelas").data("url");
                $("#tabelaParcelas").load(url, { id: venda });
                $form.removeClass("loading");
            });
        },

        TabelaParcelasReload: (vendaId) => {
            var url = $("#tabelaParcelas").data("url");
            $("#tabelaParcelas").load(url, { id: vendaId });
        },
    };

    Parcelas.Crud = {
        Atualizar: () => {
            var url = $(".btnSimStatus").data("url");
            var form = $("form.ui.form.parcela-atualizar").serialize();
            $.ajax({
                url: url,
                data: form,
                method: "POST",
                success: function (response) {
                    Parcelas.Buscar.TabelaParcelasReload(response);
                },
                erro: function () {
                    SGVL.notification("erro", "Erro ao atualizar");
                },
            });
        },
    };

    Parcelas.Buscar.ParcelasPorVenda();
    Parcelas.Buscar.TabelaParcelas();
    Parcelas.Apoio.CarregarEvento();
});