﻿$(function () {

    "use string"

    window.ImprimirContrato = window.ImprimirContrato || {};

    ImprimirContrato.FormatarNulo = function (valor) {
        return (valor !== null) ? valor : "Não informado";
    }

    ImprimirContrato.VendaEfetuada = function (url, vendaId, btn) {
        $.getJSON(url, { id: vendaId }, function (model) {

            var documento = {
                content: [
                    {
                        style: "titulo",
                        text: "LOTEAMENTO NOVO HORIZONTE - CIDADE NOVA, ESTABELECIDO NA CIDADE DE HUMAITÁ- ESTADO DO AMAZONAS.",
                        text: [
                            { text: "CONTRATO PARTICULAR DE COMPROMISSO DE COMPRA E VENDA DE IMÓVEL URBANO NÃO EDIFICADO (LOTE), PARA FINS RESIDENCIAIS OU COMERCIAIS - COM CLÁUSULA DE ALIENAÇÃO FIDUCIÁRIA (LEI Nº 9.514/97) - " },
                            { text: ImprimirContrato.FormatarNulo(model.nomeLoteamento)},
                            { text: " - " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroLoteamento) },
                            { text: ", ESTABELECIDO NA CIDADE DE " },
                            { text: ImprimirContrato.FormatarNulo(model.cidadeLoteamento) },
                            { text: "." }
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Pelo	presente	instrumento	de	CONTRATO	PARTICULAR	DE COMPROMISSO	DE	COMPRA	E	VENDA	DE	IMÓVEL	URBANO, DENOMINADO LOTEAMENTO " },
                            { text: ImprimirContrato.FormatarNulo(model.nomeLoteamento) }, { text: " - " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroLoteamento) },
                            { text: ", e na melhor forma do direito, de um lado; " },
                            { text: ImprimirContrato.FormatarNulo(model.nomeProp), bold: true }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.nascionalidadeProp) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoCivilProp) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.ocupacaoProp) }, { text: ", " },
                            { text: "portador da Cédula da Identidade nº  " },
                            { text: ImprimirContrato.FormatarNulo(model.rgProp) }, { text: " " },
                            { text: ImprimirContrato.FormatarNulo(model.orgaoExpedidorProp) }, { text: ", " },
                            { text: "inscrito no CPF sob nº " },
                            { text: ImprimirContrato.FormatarNulo(model.cpfProp) }, { text: ", " },
                            { text: "residente e domiciliado na " },
                            { text: ImprimirContrato.FormatarNulo(model.enderecoProp) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.numeroProp) }, { text: ", " },
                            { text: "Bairro " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroProp) }, { text: ", " },
                            { text: "na cidade de " },
                            { text: ImprimirContrato.FormatarNulo(model.cidadeProp) }, { text: ", " },
                            { text: "Estado do " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoProp) }, { text: ", " },
                            { text: "doravante denominado simplesmente como " },
                            { text: "VENDEDOR, ", bold: true },

                            { text: "e de outro lado à pessoa denominada no contrato anterior como sendo " },
                            { text: "Promitente Vendedor, Sr. ", bold: true },
                            { text: ImprimirContrato.FormatarNulo(model.nomeCliente), bold: true }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.nascionalidadeCliente) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoCivilCliente) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.ocupacaoCliente) }, { text: ", " },
                            { text: "portador da Cédula da Identidade nº  " },
                            { text: ImprimirContrato.FormatarNulo(model.rgCliente), bold: true }, { text: " " },
                            { text: ImprimirContrato.FormatarNulo(model.orgaoExpedidorCliente), bold: true }, { text: ", " },
                            { text: "inscrito no CPF sob nº " },
                            { text: ImprimirContrato.FormatarNulo(model.cpfCliente), bold: true }, { text: ", " },
                            { text: "residente e domiciliado na " },
                            { text: ImprimirContrato.FormatarNulo(model.enderecoCliente), bold: true }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.numeroCliente), bold: true }, { text: ", " },
                            { text: "Bairro " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroCliente), bold: true }, { text: ", " },
                            { text: "na cidade de " },
                            { text: ImprimirContrato.FormatarNulo(model.cidadeCliente) }, { text: ", " },
                            { text: "Estado do " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoCliente) }, { text: ", " },
                            { text: "doravante denominado simplesmente como " },
                            { text: "COMPRADOR(A)", bold: true },
                            { text: ", as partes acima identificadas têm, entre si, como justo e certado o presente contrato nos exatos termos e condições estabelecidas, que regerá pelas cláusulas, preço, condições de pagamento, que seguirá com cláusula de alienação fiduciária na forma delineadas abaixo:"}
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "DO OBJETO no CONTRATO", bold: true },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA PRIMEIRA: O COMPRADOR(A),", bold: true },
                            { text: " tomando . conhecimento da implantação de área de terras de lotes urbanos denominado Loteamento " },
                            { text: ImprimirContrato.FormatarNulo(model.nomeLoteamento) },
                            { text: " - " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroLoteamento) },
                            { text: ", ESTABELECIDO NA CIDADE DE " },
                            { text: ImprimirContrato.FormatarNulo(model.cidadeLoteamento) },
                            { text: ", que será implantado sobre o imóvel urbano de propriedade do VENDEDOR, com registro de Matrícula nº 366, tis. 166, Livro 2-B de Registro Geral, do Cartório do 2º Ofício, com área de 68,1103 ha, situado  na margem direita da BR 230, Km 7, sentido da cidade de Humaitá-AM a cidade de Labrea­ AM, com protocolo de requerimento de implantação n° 3460/15, junto a Prefeitura do Município de Humaitá-AM, datado em 08/06/2015, SOLICITA assegurar sua intenção de compra sobre o(s) lote(s), que após verificação in loco e da planta do loteamento, deseja assegurar como OBJETO do presente contrato a compra do " },
                            { text: "LOTE Nº ", bold: true }, { text: ImprimirContrato.FormatarNulo(model.identificacaoLote), bold: true },
                            { text: [{ text: " da Quadra ", bold: true }, { text: ImprimirContrato.FormatarNulo(model.identificacaoQuadra), bold: true }] },
                            { text: [{ text: ", com área de ", bold: true }, { text: ImprimirContrato.FormatarNulo(model.metrosQuadradosLote), bold: true }, { text: "m². ", bold: true }] },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA SEGUNDA: O COMPRADOR(A),", bold: true },
                            { text: " declara que lhe foi devidamente esclarecido, em conformidade com a documentação apresentada, projeto e demais documentos pertencentes ao loteamento, dos quais tomou conhecimento, tendo a plena ciência de que o empreendimento denominado Loteamento Novo Horizonte - Cidade Nova, encontra-se devidamente registrado em nome do VENDEDOR , estando em fase de implantação dos trâmites legais encaminhados junto à municipalidade e demais órgãos inerentes necessários para regularização do Loteamento, sendo que a liberação para construção somente ocorrerá após o pagamento de 30% (trinta por cento) do valor do imóvel ou liberação dos órgão competentes, devendo obedecer a legalidade quanto a emissão de alvarás e demais exigências legais." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Primeiro: O COMPRADOR(A), ", bold: true },
                            { text: "declara ter vistoriado o imóvel, estando ciente de suas medidas, confrontações e condições, ocorrendo a promitente compra e venda na forma 'ad corpus', sendo a partir desta data, obrigação do COMPRADOR(A) pela manutenção do imóvel nas exigências municipais, inclusive pela sua limpeza e defesa possessória em caso de esbulho e turbação." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Segundo: O COMPRADOR(A), ", bold: true },
                            { text: "declara ter ciência de pleno acordo, que a emissão definitiva de ordem de escrituração somente será procedida após a liberação dos tramites legais do loteamento." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Terceiro: O VENDEDOR, ", bold: true },
                            { text: "não se responsabiliza por promessas e/ou afirmativas que não estejam inseridas na legislação vigente ou no bojo das cláusulas contratuais deste contrato." },
                        ]
                    },

                    {
                        style: "textos",
                        text: "DA DOCUMENTAÇÃO APRESENTADA  PELO COMPRADOR(A)", bold: true
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA TERCEIRA: O COMPRADOR (A), ", bold: true },
                            { text: "assume o comprom isso de promover a entrega de todos os documentos necessários para a formalização do presente Contrato de Compromisso de Compra e Venda , bem como todos aqueles que forem necessários para a transmissão de propriedade, assim que forem requisitados, declarando por sua inteira responsabilidade, a validade e a autenticidade de todos os documentos exigidos e apresentados , declarando não possuir quaisquer informações negativas ou medidas judiciais ou extrajudiciais, que de alguma forma possa comprometer sua solvênc ia, para atender o avençado no presente instrumento." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Primeiro: ", bold: true },
                            { text: "As notificações referidas no presente contrato serão realizadas por meio de envio de carta expedida pelo VENDEDOR no endereço fornecido pelo COMPRADOR(A) e/ou podendo ser realizada por meio do endereço eletrônico de e-mail do COMPRADOR(A) ; havendo alteração do endereço, este deverá comunicar imediatamente sua  alteração,  conforme descrito no Parágrafo Segundo da Cláusula Terceira." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Segundo: ", bold: true },
                            { text: "Caso  ocorra do COMPRADOR(A), recusar o recebimento da notificação ou não se encontrar no endereço que forneceu, o ato notificatório poderá ser formalizado e tido como válido por meio de cartório ou publicação em jornal local, em uma única vez." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "DO PREÇO, FORMA E CONDIÇÕES DE PAGAMENTO", bold: true },
                        ]
                    },


                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA QUARTA: ", bold: true },
                            { text: "O imóvel objeto do presente contrato de compromisso de compra e venda, tem como preço a importância de " },
                            { text: [{ text: "R$ ", bold: true }, { text: ImprimirContrato.FormatarNulo(model.valorLote), bold: true }] },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Primeiro: ", bold: true },
                            { text: "Fica convencionado entre as partes, objetivando promover o equilíbrio contratual e manutenção do preço de mercado do imóvel, que os valores referentes às prestações vincendas , sofrerão reajuste anual, para cada ano de vigência do contrato, que ocorrerá pela utilização do IGP-M (Índice Geral de Preço de Mercado), divulgado pelo IGV (Instituto Getúlio Vargas) , incidente sobre a apuração positiva dos 12 (doze) meses anteriores, projetados para os 12 (doze) meses subsequentes e assim sucessivamente até final do pagamento. Em havendo divulgação de resultado negativo do IGP-M no período, o mesmo não será considerado. Na eventual ausência do IGP-M, será utilizado outro índice oficial que venha expressamente a substitui-lo." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Segundo: O VENDEDOR(A), ", bold: true },
                            { text: "aceitando a solicitação de prazo de carência de 1 (um) ano quanto a aplicabilidade da correção , ou seja, 12 (doze) meses, concede o prazo de carência solicitado." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Terceiro: ", bold: true },
                            { text: "Resta convencionado que os pagamentos assumidos pelo COMPRADOR(A) , se dará mediante boleto bancário, com emissão a cada ano, ou seja, a cada ano será emitido 12 (doze) boletos, ficando a cargo do COMPRADOR(A) o comparecimento até o escritório do VENDEDOR para promover o recebimento dos mesmos, restando sob responsabilidade exclusiva do COMPRADOR(A), a guarda e manutenção dos comprovantes de pagamento, para apresentação quando solicitado a fim de demonstrar a quitação do preço." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "DO PAGAMENTO ANTECIPADO", bold: true },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA QUINTA: O COMPRADOR(A), ", bold: true },
                            { text: "poderá, caso tenha interesse, amortizar total ou parcialmente o saldo devedor remanescente, desde  que  o faça com seu valor atualizado pelo índice contratual até a data do efetivo pagamento . Havendo pagamento antecipado de no mínimo 5 (cinco)  parcelas subsequentes, o mesmo terá direito a um desconto equivalente a 2,5% (dois virgula cinco por cento), sobre o valor das parcelas, e, no caso de pagamento antecipado da integralidade das parcelas, que não poderão ser menos que 5 (cinco) parcelas para atingir o seu término, o mesmo terá direito a um desconto no valor de 5% (cinco por cento), sobre o valor das parcelas." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "DA INADIMPLÊNCIA  E DA MORA", bold: true },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA SEXTA: ", bold: true },
                            { text: "No caso de inadimplemento de 3 (três) parcelas mensais, estará caracterizada de pleno direito a mora do COMPRADOR(A), independentemente de notificação judicial ou extrajudicial o presente contrato restará automaticamente rescindido, consolidando por consequente a propriedade do imóvel em nome do VENDEDOR que poderá adotar as medidas legais cabíveis." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Primeiro: ", bold: true },
                            { text: "Após o prazo de carência de 90 (noventa) dias concedido para caracterização da mora rescisória do contrato, ou seja, 3 (três) meses de atraso no vencimento das parcelas, o VENDEDOR , no prazo máximo de 30 (trinta) dias, promoverá a intimação do COMPRADOR(A), por meio do Oficial do competente Registro de Imóveis, para satisfazer a purgação da mora, no prazo de 15 (quinze) dias, a prestação ou prestações vencida(s) até a data do pagamento, acrescidas de juros convencionais, das penalidades e demais encargos contratuais e legais, inclusive tributos de qualquer natureza incidentes sobre o imóvel, além das despesas de cobrança e de intimação, consoante determinação do art. 26, § 1º, da Lei 9.514/97." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Segundo: ", bold: true },
                            { text: "Purgada a mora pelo COMPRADOR(A) no Registro de Imóveis, nos termos dispostos no § 5º, do art. 26, da Lei   9.514/97, convalescerá o contrato de alienação fiduciária." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Terceiro: ", bold: true },
                            { text: "Caso não haja a purgação da mora, responde o COMPRADOR(A) pelo pagamento dos impostos, taxas e quaisquer outros encargos que recaiam ou venham a recair sobre o imóvel, cuja posse tenha sido transferida ao VENDEDOR, até a data em que vier a ser imitido na posse." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Parágrafo Quarto: ", bold: true },
                            { text: "Decorrido o prazo sem a purgação da mora, o Oficial do competente Registro de Imóveis, certificará o fato, ato contínuo promoverá a averbação na matrícula do imóvel, da consolidação da propriedade em nome do VENDEDOR, à vista da prova do pagamento por este, do imposto de transmissão inter vivos." },
                        ]
                    },

                    {
                        style: "assinatura",
                        text: [
                            { text: [{ text: "\n\nCOMPRADOR(A)/FIDUCIANTE\n", bold: true }, { text: ImprimirContrato.FormatarNulo(model.nomeCliente), bold: true }] },
                            { text: [{ text: "\n\n\n\n\n\nVENDEDOR/FIDUCIÁRIO\n", bold: true }, { text: ImprimirContrato.FormatarNulo(model.nomeProp), bold: true }] },
                        ]
                    },
                ],

                styles: {
                    titulo: {
                        fontSize: 12,
                        margin: [30, 30, 30, 30],
                        fontFamily: 'arial',
                        alignment: 'justify',
                        bold: true,
                    },
                    textos: {
                        fontSize: 12,
                        margin: [30, 0, 30, 20],
                        fontFamily: 'arial',
                        alignment: 'justify',
                    },
                    assinatura: {
                        fontSize: 12,
                        margin: [320, 30, 30, 20],
                        fontFamily: 'arial',
                        alignment: 'center',
                    },
                }
            };
            btn.removeClass("loading");
            pdfMake.createPdf(documento).open();
        });
    };

    $(document).on("click", "#btnImprimirContratoVendaEfetuada", function () {
        var btn = $(this);
        btn.addClass("loading");
        var vendaId = $("#VendaId").val();
        var url = $("#VendaId").data("url");

        ImprimirContrato.VendaEfetuada(url, vendaId, btn)
    });
});