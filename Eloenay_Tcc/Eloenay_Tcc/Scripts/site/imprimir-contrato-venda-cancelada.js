﻿$(function () {

    "use string"

    window.ImprimirContrato = window.ImprimirContrato || {};

    ImprimirContrato.FormatarNulo = function (valor) {
        return (valor !== "") ? valor : "Não informado";
    }

    ImprimirContrato.VendaCancelada = function (vendaId, btn) {
        $.getJSON(vendaId, function (model) {

            var documento = {
                content: [
                    {
                        style: "titulo",
                        text: "INSTRUMENTO PARTICULAR DE DISTRATO AMIGÁVEL DE CONTRATO DE COMPROMISSO DE COMPRA E VENDA DE IMÓVEL"
                    },
                    {
                        style: "textos",
                        text: [
                            { text: "O presente instrumento de " }, { text: "DISTRATO CONTRATUAL AMIGÁVEL, ", bold: true },
                            { text: "que se faz entre as partes neste ato e que tem como objetivo rescindir o " },
                            { text: "CONTRATO DE COMPROMISSO DE COMPRA E VENDA DE IMÓVEL, ", bold: true },
                            { text: "em conformidade com o disposto na Cáusula Sexta, do Contrado firmado " },
                            { text: ImprimirContrato.FormatarNulo(model.dataCompra) },
                            { text: ", que passa a ser parte integrante deste instrumento." }
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "Por este instrumento particular, de um lado à pessoa denominada no contrato anterior como sendo " },
                            { text: "Promitente Vendedor, Sr. ", bold: true },
                            { text: ImprimirContrato.FormatarNulo(model.nomeProp), bold: true }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.nascionalidadeProp) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoCivilProp) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.ocupacaoProp) }, { text: ", " },
                            { text: "portador da Cédula da Identidade nº  " },
                            { text: ImprimirContrato.FormatarNulo(model.rgProp) }, { text: " " },
                            { text: ImprimirContrato.FormatarNulo(model.orgaoExpedidorProp) }, { text: ", " },
                            { text: "inscrito no CPF sob nº " },
                            { text: ImprimirContrato.FormatarNulo(model.cpfProp) }, { text: ", " },
                            { text: "residente e domiciliado na " },
                            { text: ImprimirContrato.FormatarNulo(model.enderecoProp) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.numeroProp) }, { text: ", " },
                            { text: "Bairro " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroProp) }, { text: ", " },
                            { text: "na cidade de " },
                            { text: ImprimirContrato.FormatarNulo(model.cidadeProp) }, { text: ", " },
                            { text: "Estado do " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoProp) }, { text: ", " },
                            { text: "doravante denominado simplesmente como " },
                            { text: "PRIMEIRO DISTRANTANTE, ", bold: true },

                            { text: "e de outro lado à pessoa denominada no contrato anterior como sendo " },
                            { text: "Promitente Vendedor, Sr. ", bold: true },
                            { text: ImprimirContrato.FormatarNulo(model.nomeCliente), bold: true }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.nascionalidadeCliente) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoCivilCliente) }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.ocupacaoCliente) }, { text: ", " },
                            { text: "portador da Cédula da Identidade nº  " },
                            { text: ImprimirContrato.FormatarNulo(model.rgCliente), bold: true }, { text: " " },
                            { text: ImprimirContrato.FormatarNulo(model.orgaoExpedidorCliente), bold: true }, { text: ", " },
                            { text: "inscrito no CPF sob nº " },
                            { text: ImprimirContrato.FormatarNulo(model.cpfCliente), bold: true }, { text: ", " },
                            { text: "residente e domiciliado na " },
                            { text: ImprimirContrato.FormatarNulo(model.enderecoCliente), bold: true }, { text: ", " },
                            { text: ImprimirContrato.FormatarNulo(model.numeroCliente), bold: true }, { text: ", " },
                            { text: "Bairro " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroCliente), bold: true }, { text: ", " },
                            { text: "na cidade de " },
                            { text: ImprimirContrato.FormatarNulo(model.cidadeCliente) }, { text: ", " },
                            { text: "Estado do " },
                            { text: ImprimirContrato.FormatarNulo(model.estadoCliente) }, { text: ", " },
                            { text: "doravante denominado simplesmente como " },
                            { text: "SEGUNDA DISTRANTANTE.", bold: true },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "As partes acima Identificadas têm como justo e acertado o presente " },
                            { text: "INSTRUMENTO PARTICULAR DE DISTRATO CONTRATUAL AMIGÁVEL, ", bold: true },
                            { text: "declarando-se por satisfeitas pelas disposições constantes nas cláusulas descritas abaixo, sob as quais as partes mutuamente se obrigam a cumprir nos seguintes termos:" },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA PRIMEIRA: ", bold: true },
                            { text: "As partes, em comum acordo de vontade, nos termos dispostos na Cláusula Décima, do contrato inicial, resolvem, a pedido da " },
                            { text: "SEGUNDA DISTRATANTE, ", bold: true },
                            { text: "rescindir de forma amigável o vínculo contratual que lhes uniam anteriormente no " },
                            { text: "Instrumento de Contrato de Compromisso de Compra e Venda de Imóvel, ", bold: true },
                            { text: "firmado em data de " },
                            { text: ImprimirContrato.FormatarNulo(model.dataCompra) },
                            { text: ", tendo como objeto a compra de 01 (um) " },
                            { text: "Lote de Terra Urbano nº ", bold: true },
                            { text: ImprimirContrato.FormatarNulo(model.identificacaoLote), bold: true },
                            { text: [{ text: " da Quadra ", bold: true }, { text: ImprimirContrato.FormatarNulo(model.identificacaoQuadra), bold: true }] },
                            { text: [{ text: ", com área de ", bold: true }, { text: ImprimirContrato.FormatarNulo(model.metrosQuadradosLote), bold: true }, { text: "m². ", bold: true }] },
                            { text: "no Loteamento denominado " },
                            { text: ImprimirContrato.FormatarNulo(model.nomeLoteamento) }, { text: " - " },
                            { text: ImprimirContrato.FormatarNulo(model.bairroLoteamento) },
                            { text: ", estabelecido nesta Cidade de " },
                            { text: ImprimirContrato.FormatarNulo(model.cidadeLoteamento) },
                            { text: ", implantado sobre o imóvel urbano de propriedade do PRIMEIRO DISTRATANTE, com registro de matrícula nº " },
                            { text: ImprimirContrato.FormatarNulo(model.matriculaLoteamento) },
                            { text: ", fls 166, Livro 2-B de Registro Geral do Cartório do 2° Ofício, situado na(o) " },
                            { text: ImprimirContrato.FormatarNulo(model.enderecoLoteamento) },
                            { text: ", com protocolo de Requerimento deImplantação junto a municipalidade autuado sob    o nº 3460/15, datado em 08/06/2015." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA SEGUNDA: A SEGUNDA DISTRATANTE, ", bold: true },
                            { text: "em data de " },
                            { text: ImprimirContrato.FormatarNulo(model.dataCancelamento) },
                            { text: ", compareceu perante o PRIMEIRO DISTRATANTE, para  relatar que, em razão  de questões financeiras,  não mais teria condições de adimplir com sua obrigação relativa ao pagamento das parcelas convencionadas, razão pela  qual,  nos  termos  dispostos  na  Cláusula  Décima, do  contrato  anterior, solicitou que fosse realizada de forma amigável a " },
                            { text: "RESCISÃO CONTRATUAL, ", bold: true },
                            { text: "por  meio  do ·ftrmamento  do  presente " },
                            { text: "Instrumento  Particular  de  Distrato Amigável de Contrato de Compromisso de Compra e Venda de Imóvel, ", bold: true },
                            { text: "em conformidade com a observância das demais cláusulas contratuais firmadas." },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "DAS CONSIDERAÇÕES DO DISTRATO", bold: true },
                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA TERCEIRA: ", bold: true },
                            { text: "Considerando que o " },
                            { text: "CONTRATO INICIAL ", bold: true },
                            { text: "estabelece em sua Cláusula Déci.ma que, em ocorrendo a desistência do negócio, a parte que der causa, ficará obrigada a pagar a parte contrária a título de danos, multa penal na ordem de " },
                            { text: ImprimirContrato.FormatarNulo(model.multa) },
                            { text: "% , sobre o valor já pago;" },

                        ]
                    },

                    {
                        style: "textos",
                        text: [
                            { text: "CLÁUSULA QUARTA: ", bold: true },
                            { text: "Considerando que o imóvel negociado fora comercializado no valor de " },
                            { text: [{ text: "R$ "}, { text: ImprimirContrato.FormatarNulo(model.valorLote)}] },
                            { text: "considerando que a SEGUNDA DISTRATANTE promoveu até a presente data o pagamento do valor a título de entrada de R$ " },
                            { text: ImprimirContrato.FormatarNulo(model.valorEntrada) },
                            { text: [{ text: " mais "}, { text: ImprimirContrato.FormatarNulo(model.parcelasPagas)}] },
                            { text: [{ text: " parcelas individuais no valor de R$ " }, { text: ImprimirContrato.FormatarNulo(model.valorParcelas) }] },
                            { text: [{ text: ", somadas perfaz o valor de R$ " }, { text: ImprimirContrato.FormatarNulo(model.valorDevolucao) }] },
                            { text: "Considerando que o Contrato Principal estabelece em sua Cláusula Décima que, em ocorrendo a desistência do negócio, a parte que der causa ficará obrigada a pagar a parte contrária, a título de danos, multa penal na ordem de " },
                            { text: [{ text: ImprimirContrato.FormatarNulo(model.multa) }, { text: "% , sobre o valor já pago, deverá ser descontado o valor de R$ " }] },
                            { text: ImprimirContrato.FormatarNulo(model.valorDesconto) },
                            { text: "considerando a somatória dos valores já pagos, realizados os devidos abatimentos contratuais, resta como valor a restituir na ordem de R$" },
                            { text: ImprimirContrato.FormatarNulo(model.valorDevolucao) },
                            { text: [{ text: ", o saldo remanescente será restituído em " }, { text: ImprimirContrato.FormatarNulo(model.parcelasPagas) }] }, 
                            { text: [{ text: " parcelas mensais de R$ " }, { text: ImprimirContrato.FormatarNulo(model.valorParcelas) }] },
                            { text: [{ text: " parcelas mensais de R$ " }, { text: ImprimirContrato.FormatarNulo(model.valorParcelas) }] },
                            { text: [{ text: " com início do vencimento da 1º (primeira) parcela em data de " }, { text: ImprimirContrato.FormatarNulo(model.primeiraParcela) }] },
                            { text: [{ text: ", até pagamento final que se dará em data de " }, { text: ImprimirContrato.FormatarNulo(model.ultimaParcela) }] },
                        ]
                    },

                    {
                        style: "assinatura",
                        text: [
                            { text: [{ text: "\n\nCOMPRADOR(A)/FIDUCIANTE\n", bold: true }, { text: ImprimirContrato.FormatarNulo(model.nomeCliente), bold: true }] },
                            { text: [{ text: "\n\n\n\n\n\nVENDEDOR/FIDUCIÁRIO\n", bold: true }, { text: ImprimirContrato.FormatarNulo(model.nomeProp), bold: true }] },
                        ]
                    },
                ],

                styles: {
                    titulo: {
                        fontSize: 12,
                        margin: [30, 30, 30, 30],
                        fontFamily: 'arial',
                        alignment: 'center',
                        bold: true,
                    },
                    textos: {
                        fontSize: 12,
                        margin: [30, 0, 30, 20],
                        fontFamily: 'arial',
                        alignment: 'justify',
                    },
                    assinatura: {
                        fontSize: 12,
                        margin: [300, 30, 30, 20],
                        fontFamily: 'arial',
                        alignment: 'center',
                    },
                }
            };
            btn.removeClass("loading");
            pdfMake.createPdf(documento).open();
        });
    };

    $(document).on("click", "#btnImprimirContratoVendaCancelada", function () {
        var btn = $(this);
        btn.addClass("loading");
        var vendaId = $("#VendaId").data("url");
        ImprimirContrato.VendaCancelada(vendaId, btn)
    });
});