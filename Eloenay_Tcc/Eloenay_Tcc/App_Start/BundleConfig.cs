﻿using System.Web.Optimization;

namespace Eloenay_Tcc
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            #region JS
            bundles.Add(new ScriptBundle("~/bundles/jqueryval-js").Include(
                      "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui-js").Include(
                      "~/Scripts/jquery-ui-1.10.4.custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatable-js").Include(
                      "~/Scripts/site/datatable.js"));

            bundles.Add(new ScriptBundle("~/bundles/autocomplete-js").Include(
                      "~/Scripts/site/jquery.ui.autocomplete.js"
          ));

            bundles.Add(new ScriptBundle("~/bundles/basic-js").Include(
                      "~/Scripts/jquery-{version}.js",
                      "~/Scripts/semantic.js",
                      "~/Scripts/lobibox.js",
                      "~/Scripts/site/layout.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker-js").Include(
                      "~/Scripts/DatePicker/datepicker.js",
                      "~/Scripts/site/datepicker.config.js"));


            bundles.Add(new ScriptBundle("~/bundles/mask-js").Include(
                      "~/Scripts/jquery.mask.js",
                      "~/Scripts/jquery.priceformat.js",
                      "~/Scripts/site/mask.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr-js").Include(
                      "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/pdfmake-js").Include(
                      "~/Scripts/pdfmake.min.js",
                      "~/Scripts/vfs_fonts.js"));

            #endregion

            #region CSS
            bundles.Add(new StyleBundle("~/Content/paged-list-css").Include(
                      "~/Content/PagedList.css"));

            bundles.Add(new StyleBundle("~/Content/datepicker-css").Include(
                      "~/Content/DatePicker/datepicker.css"));

            bundles.Add(new StyleBundle("~/Content/autocomplete-css").Include(
                      "~/Content/jquery.ui.autocomplete.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/basic-css").Include(
                      "~/Content/semantic.css",
                      "~/Content/components/icon.css",
                      "~/Content/lobibox.css",
                      "~/Content/basic/layout.css",
                      "~/Content/basic/responsive.css",
                      "~/Content/jquery-ui-1.10.4.custom.css"));

            bundles.Add(new StyleBundle("~/Content/site-css").Include(
                      "~/Content/basic/site.css"));

            bundles.Add(new StyleBundle("~/Content/login-css").Include(
                      "~/Content/semantic.css",
                      "~/Content/lobibox.css",
                      "~/Content/basic/login.css"));
            #endregion

            #region Meus JS

            bundles.Add(new ScriptBundle("~/bundles/calculos-js").Include(
                      "~/Scripts/site/calculos.js"));

            bundles.Add(new ScriptBundle("~/bundles/menu-layout-js").Include(
                      "~/Scripts/site/menu-layout.js"));

            bundles.Add(new ScriptBundle("~/bundles/parcelas-js").Include(
                      "~/Scripts/site/parcelas.js"));

            bundles.Add(new ScriptBundle("~/bundles/venda-js").Include(
                      "~/Scripts/site/venda.js"));

            bundles.Add(new ScriptBundle("~/bundles/adicionar-venda-js").Include(
                      "~/Scripts/site/adicionar-venda.js"));

            bundles.Add(new ScriptBundle("~/bundles/lote-js").Include(
                      "~/Scripts/site/lote.js"));

            bundles.Add(new ScriptBundle("~/bundles/validar-campos-js").Include(
                      "~/Scripts/site/validar-campos.js"));

            bundles.Add(new ScriptBundle("~/bundles/preencher-drop-js").Include(
                      "~/Scripts/site/preencher-drop.js"));

            bundles.Add(new ScriptBundle("~/bundles/imprimir-contrato-venda-efetuada-js").Include(
                      "~/Scripts/site/imprimir-contrato-venda-efetuada.js"));

            bundles.Add(new ScriptBundle("~/bundles/imprimir-contrato-venda-cancelada-js").Include(
                      "~/Scripts/site/imprimir-contrato-venda-cancelada.js"));

            bundles.Add(new ScriptBundle("~/bundles/login-js").Include(
                      "~/Scripts/site/login.js"));

            bundles.Add(new ScriptBundle("~/bundles/parcelas-por-periodo-js").Include(
                      "~/Scripts/site/parcelas-por-periodo.js"));
            #endregion
        }
    }
}