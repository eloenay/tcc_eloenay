﻿using System;
using Ninject;

namespace Eloenay_Tcc.Intefaces.Repositories
{
    public interface IRepositoryBase<TEntity> : IDisposable where TEntity : class
    {
        TEntity Adicionar(TEntity entity);
        TEntity Atualizar(TEntity entity);
    }
}