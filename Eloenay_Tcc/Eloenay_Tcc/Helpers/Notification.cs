﻿using System.Web.Mvc;

namespace Eloenay_Tcc.Helpers
{
    public class Notification : ActionResult
    {
        private readonly ActionResult _actionResult;
        private readonly string _message;
        private readonly MessageType _type;

        public Notification(ActionResult actionResult, string message, MessageType tipo)
        {
            _actionResult = actionResult;
            _message = message;
            _type = tipo;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            switch (_type)
            {
                case MessageType.Success:
                    context.Controller.TempData["Success"] = _message;
                    break;
                case MessageType.Error:
                    context.Controller.TempData["Error"] = _message;
                    break;
                case MessageType.Alert:
                    context.Controller.TempData["Alert"] = _message;
                    break;
                case MessageType.Information:
                    context.Controller.TempData["Information"] = _message;
                    break;
                default:
                    context.Controller.TempData["Error"] = _message;
                    break;
            }
            _actionResult.ExecuteResult(context);
        }

        public enum MessageType
        {
            Success = 1,
            Error = 2,
            Alert = 3,
            Information = 4,
        }
    }
    public static class ActionResultExtensions
    {
        public static ActionResult Success(this ActionResult actionResult, string message)
        {
            return new Notification(actionResult, message, Notification.MessageType.Success);
        }
        public static ActionResult Error(this ActionResult actionResult, string message)
        {
            return new Notification(actionResult, message, Notification.MessageType.Error);
        }
        public static ActionResult Alert(this ActionResult actionResult, string message)
        {
            return new Notification(actionResult, message, Notification.MessageType.Alert);
        }
        public static ActionResult Information(this ActionResult actionResult, string message)
        {
            return new Notification(actionResult, message, Notification.MessageType.Information);
        }
    }
}